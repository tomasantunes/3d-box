# HappieEngine

Graphics Engine with C++ and OpenGL - work in progress.

## Build Instructions

- Download [Freeglut](http://freeglut.sourceforge.net)

- Download [Glew](http://glew.sourceforge.net)

- Download [GLM](https://github.com/g-truc/glm/tags)

- Download [Assimp](https://github.com/assimp/assimp/releases/tag/v3.3.1/)

- Download [STB](https://github.com/nothings/stb)

- Download [ImGui](https://github.com/ocornut/imgui)

- Download [ImGuiFileDialog](https://github.com/aiekick/ImGuiFileDialog)

- Open solution file in Visual Studio

- Go to Project->Properties

- Go to the tab C/C++->General and add Additional Include Directories

- Go to the tab Linker->General and add Additional Library Directories

- Go to the tab Linker->Input and add opengl32.lib, glew32.lib, glu.lib, freeglut.lib and assimp.lib

- Add the following files to the same directory as the executable: freeglut.dll, glew32.dll, assimp-vc140-mt.dll

[![paypal](https://i.imgur.com/URcFCl1.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=Y4DJVGBKXB8MJ&source=url)

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/Y8Y2M1UI)