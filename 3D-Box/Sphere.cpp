using namespace std;
#include "Sphere.h"
#include <iostream>
#include <math.h>
#include <cmath>
#include "Math.h"
#include <vector>
#include "Vector3D.h"
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#define M_PI 3.14159265358979323846


Sphere::Sphere(double radius, int slices, int stacks, Vector3D position, vector<float> color) : radius(radius), slices(slices), stacks(stacks), position(position), color(color)  {
	scale = 1.0;
	verts = {};

	float x, y, z, xy;
	float nx, ny, nz, lengthInv = 1.0f / radius;
	float s, t;

	float sectorStep = 2 * M_PI / slices;
	float stackStep = M_PI / stacks;
	float sectorAngle, stackAngle;

	for (int i = 0; i <= stacks; ++i) {
		stackAngle = M_PI / 2 - i * stackStep;
		xy = radius * cosf(stackAngle);
		z = radius * sinf(stackAngle);

		for (int j = 0; j <= slices; ++j) {
			sectorAngle = j * sectorStep;
			x = xy * cosf(sectorAngle);
			y = xy * sinf(sectorAngle);
			verts.push_back(Vector3D(x, y, z));
		}
	}
}

void Sphere::Render() {

}

vector<Vertex> Sphere::GetVertices() {
	vector<Vertex> vertices = {};
	for (int i = 0; i < verts.size(); i++) {
		Vertex vertex;
		glm::vec2 texCoords;
		vertex.Position = glm::vec3(verts[i].x, verts[i].y, verts[i].z);
		texCoords = glm::vec2(0.f, 0.f);
		vertex.TexCoords = texCoords;
		vertex.Normal = glm::vec3(1.0f, 0.f, 0.f);
		vertex.Color = glm::vec3(1.f, 0.f, 0.f);
		vertex.selected = false;
		vertices.push_back(vertex);
	}
	return vertices;
}

vector<unsigned int> Sphere::GetIndices() {
	vector<unsigned int> indices = {};
	int k1, k2;
	for (int i = 0; i < stacks; ++i) {
		k1 = i * (slices + 1);
		k2 = k1 + slices + 1;
		for (int j = 0; j < slices; ++j, ++k1, ++k2)
		{
			if (i != 0)
			{
				indices.push_back(k1);
				indices.push_back(k2);
				indices.push_back(k1 + 1);
			}
			if (i != (stacks - 1))
			{
				indices.push_back(k1 + 1);
				indices.push_back(k2);
				indices.push_back(k2 + 1);
			}
		}
	}
	return indices;
}



void Sphere::updateScale(float f) {
	float prev = scale;
	float next = scale + f;
	float diff = next / prev;
	radius *= diff;
	scale += f;
}

void Sphere::setColor(vector<float> col) {
	color = col;
}

void Sphere::setPosition(Vector3D vec) {
	position = vec;
}

Sphere::~Sphere() {

}
