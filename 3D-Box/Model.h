#pragma once
#include "Mesh.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <iostream>
#include "structs.cpp"


class Model
{
	public:
		/*  Functions   */
		Model(string path, string directory, Shader shader);
		vector<Texture> textures_loaded;
		vector<Mesh> meshes;
		string directory;
		GLenum format;
		Shader shader;
		Shader shaderSingleColor;
		bool gammaCorrection;
		~Model();
		void Draw();
		void SetPosition(glm::vec3 position);
	private:
		/*  Functions   */
		void loadModel(string path, string directory);
		void processNode(aiNode* node, const aiScene* scene);
		Mesh processMesh(aiMesh* mesh, const aiScene* scene);
		unsigned int TextureFromFile(const char* path, string directory, bool gamma);
		vector<Texture> Model::loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName);
};

