#pragma once
using namespace std;
#include "Vector3D.h"
#include <GL/glew.h>
#include <gl/freeglut.h>
#include "Matrix.h"
#include "Math.h"
#include "Math.h"
#include <vector>
#include "structs.cpp"

class Sphere
{
public:
	Sphere(double radius, int slices = 16, int stacks = 16, Vector3D position = Vector3D(0.f, 0.f, 0.f), vector<float> color = { 0.5f, 0.5f, 0.5f });
	~Sphere();
	float scale;
	float radius;
	float stacks;
	float slices;
	Vector3D position;
	void setPosition(Vector3D vec);
	void setColor(vector<float> col);
	void updateScale(float f);
	void Render();
	vector<Vector3D> verts;
	vector<Vertex> GetVertices();
	vector<unsigned int> GetIndices();

private:
	vector<float> color;
};

