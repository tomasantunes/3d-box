using namespace std;
#pragma once
#include <GL/glew.h>
#include <gl/freeglut.h>
#include "Matrix.h"
#include "Vector3D.h"
#include "Math.h"
#include "Box.h"
#include "Sphere.h"
#include "Cylinder.h"
#include "Model.h"
#include "Shader.h"
#include "Plane.h"
#include "Texture.h"
#include <iostream>
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include <gtx/closest_point.hpp>
#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <iostream>
#include <fstream>

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include "imgui.h"
#include "imgui.cpp"
#include "imgui_draw.cpp"
#include "imgui_demo.cpp"
#include "imgui_widgets.cpp"
#include "imgui_internal.h"
//#include "imstb_rectpack.h"
//#include "imstb_textedit.h"
//#include "imstb_truetype.h"
#include "imconfig.h"
#include "imgui_impl_glut.h"
#include "imgui_impl_opengl2.h"
#include "imgui_impl_glut.cpp"
#include "imgui_impl_opengl2.cpp"
#include "ImGuiFileDialog.h"
#include "ImGuiFileDialog.cpp"
#include "ImGuiSettings.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define MAX_SPEED 0.4
#define ACCELERATION 0.1
#define ZOOM_SPEED 0.001
#define PI 3.1415f

//CAMERA 
Vector3D centerPosition = Vector3D(0.f, 0.f, 0.f);
Vector3D eyePosition = Vector3D(5.f, 5.f, 5.f);
Vector3D upVector = Vector3D(0.f, 0.f, 1.f);
glm::vec3 cameraDir = glm::vec3(-5.f, -5.f, -5.f);

//CUBE
bool isMoving = false;
bool isRotating = false;
bool isScaling = false;
float speed = 0.f;
float rotationX = 0.f;
float rotationY = 0.f;
float rotationZ = 0.f;
float rotation_theta = 0.f;
float rotation_phi = 0.f;
float rotate_scene = 0.f;
float scale_op = 0.1f;
int window_w = 1920;
int window_h = 1080;
int scene_w = window_w;
int scene_h = window_h - 150;
int mouse_x;
int mouse_y;
bool mouse_down;
bool gui_active = true;
Vector3D direction = Vector3D(1.f, 0.f, 0.f);
vector<Model> models = {};
vector<Mesh> meshes = {};
vector<Texture> images = {};
GLuint* image;

//VIEW
float zoom = 0.01f;
float rotateAngle = 0.f;
float pitch = 0.f;
int nFPS = 30;

const char* vertexStr = "#version 330 core\n"
"layout(location = 0) in vec3 aPos;\n"
"layout(location = 1) in vec3 aNormal;\n"
"layout(location = 2) in vec2 aTexCoords;\n"
"out vec2 TexCoords;\n"
"out vec3 Normal;\n"
"out vec3 Pos;\n"
"uniform mat4 model;\n"
"uniform mat4 view;\n"
"uniform mat4 projection;\n"
"void main()\n"
"{\n"
	"TexCoords = aTexCoords;\n"
	"gl_Position = projection * (view * model) * vec4(aPos, 1.0);\n"
	"Normal = aNormal;\n"
	"Pos = aPos;\n"
"}\n";

const char* vertex2DStr = "#version 330 core\n"
"layout(location = 0) in vec3 aPos;\n"
"layout(location = 1) in vec3 aNormal;\n"
"layout(location = 2) in vec2 aTexCoords;\n"
"out vec2 TexCoords;\n"
"out vec3 Normal;\n"
"uniform mat4 model;\n"
"uniform mat4 view;\n"
"uniform mat4 projection;\n"
"void main()\n"
"{\n"
"TexCoords = aTexCoords;\n"
"gl_Position = projection * vec4(aPos, 1.0);\n"
"Normal = aNormal;\n"
"}\n";

const char* vertex2DSimpleStr = "#version 330 core\n"
"layout(location = 0) in vec3 aPos;\n"
"layout(location = 1) in vec3 aNormal;\n"
"layout(location = 2) in vec2 aTexCoords;\n"
"uniform mat4 projection;\n"
"out vec2 TexCoords;\n"
"void main()\n"
"{\n"
	"gl_Position = projection * vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
	"TexCoords = aTexCoords;\n"
"}\0";

const char* fragment2DSimpleStr = "#version 330 core\n"
"out vec4 FragColor;\n"
"in vec2 TexCoords;\n"
"uniform sampler2D texture_diffuse1;\n"
"void main()\n"
"{\n"
"   FragColor = texture(texture_diffuse1, TexCoords);\n"
"}\n\0";

const char* fragmentStr = "#version 330 core\n"
"out vec4 FragColor;\n"
"in vec2 TexCoords;\n"
"in vec3 Normal;\n"
"in vec3 Pos;\n"
"uniform sampler2D texture_diffuse1;\n"
"uniform vec3 lightPos;\n"
"uniform vec3 lightColor;\n"
"uniform vec3 viewPos;\n"
"void main()\n"
"{\n"
	"vec3 norm = normalize(Normal);\n"
	"vec3 lightDir = normalize(lightPos - Pos);\n"
	"float diff = max(dot(norm, lightDir), 0.0);\n"
	"vec3 diffuse = diff * lightColor;\n"
	"float ambientStrength = 0.5;\n"
	"vec3 ambient = ambientStrength * lightColor;\n"

	"float specularStrength = 0.5;\n"
	"vec3 viewDir = normalize(viewPos - Pos);\n"
	"vec3 reflectDir = reflect(-lightDir, norm);\n"
	"float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);\n"
	"vec3 specular = specularStrength * spec * lightColor;\n"

	"vec4 result = (vec4(ambient, 1.0) + vec4(diffuse, 1.0)) * texture(texture_diffuse1, TexCoords);\n"
	"FragColor = result;\n"
"}\n";

const char* singleColorFragmentStr = "#version 330 core\n"
"uniform vec3 color_in;\n"
"out vec4 FragColor;\n"
"void main()\n"
"{\n"
"FragColor = vec4(color_in.x, color_in.y, color_in.z, 1.0);\n"
"}\n";

Shader shader1;
Shader shader2D;
Shader shader2DSimple;
ImGuiSettings ig;
GLuint tex;
GLuint tex2;
GLuint fbo;
unsigned int rbo;

void init(void) {
	shader1.load(vertexStr, fragmentStr);
	shader2D.load(vertex2DStr, singleColorFragmentStr);
	shader2DSimple.load(vertex2DSimpleStr, fragment2DSimpleStr);
	ig = ImGuiSettings();
}
/*
void print(string str) {
	std::cout << str << std::endl;
}

void printf(float f) {
	std::cout << f << std::endl;
}
*/

void rotateScene() {
	glm::mat4 model = glm::mat4(1.0);
	glm::vec4 v4In(eyePosition.x, eyePosition.y, eyePosition.z, 1.0f);
	glm::vec4 v4RotCenter(0.f, 0.f, 0.f, 0.f);
	glm::vec3 v3RotAxis(0.0f, 0.0f, 1.0f);
	glm::mat4x4 matRot = glm::rotate(model, glm::radians(rotate_scene), v3RotAxis);
	glm::vec4 b = Math::rotateAround(v4In, v4RotCenter, matRot, model);
	glm::vec3 c = glm::vec3(b.x, b.y, b.z);
	eyePosition = Vector3D(c.x, c.y, c.z);
}

glm::mat4 getCurrentView() {
	//std::cout << ig.view << std::endl;
	if (ig.view == "front") {
		eyePosition = Vector3D(5.f, 0.f, 0.f);
	}
	else if (ig.view == "back") {
		eyePosition = Vector3D(-5.f, 0.f, 0.f);
	}
	else if (ig.view == "left") {
		eyePosition = Vector3D(0.f, -5.f, 0.f);
	}
	else if (ig.view == "right") {
		eyePosition = Vector3D(0.f, 5.f, 0.f);
	}
	else if (ig.view == "top") {
		eyePosition = Vector3D(1.f, 1.f, 25.f);
	}
	else if (ig.view == "bottom") {
		eyePosition = Vector3D(1.f, 1.f, -25.f);
	}
	else if (ig.view == "perspective"){
		eyePosition = Vector3D(5.f, 5.f, 5.f);
		rotateScene();
	}
	else if (ig.view == "ortho") {
		eyePosition = Vector3D(5.f, 5.f, 5.f);
		rotateScene();
	}
	glm::mat4 view = glm::lookAt(glm::vec3(eyePosition.x, eyePosition.y, eyePosition.z), glm::vec3(centerPosition.x, centerPosition.y, centerPosition.z), glm::vec3(upVector.x, upVector.y, upVector.z));
	return view;
}

glm::mat4 getCurrentProjection() {
	float ratio = scene_w / scene_h;
	//std::cout << ig.projection << std::endl;
	if (ig.projection == "ortho") {
		return glm::ortho(-25.f * ratio, 25.f * ratio, 25.f, -25.f, -25.f, 25.f);
	}
	else {
		return glm::perspective(45.f, (float)scene_w / (float)scene_h, 0.1f, 25.0f);
	}
}

void rotateSceneLeft() {
	rotate_scene -= 1.f;
}

void rotateCameraUp() {
	pitch += 0.001f;
	glm::vec3 up = glm::vec3(upVector.x, upVector.y, upVector.z);
	cameraDir = (cameraDir * cos(0.001f) + (up * cos(0.001f)));
	glm::vec3 rightVector = Math::cross3(cameraDir, up);
	glm::vec3 center = glm::vec3(eyePosition.x, eyePosition.y, eyePosition.z);
	center += cameraDir;
	centerPosition = Vector3D(center.x, center.y, center.z);
	//std::cout << "x = " << center.x << ", y = " << center.y << ", z = " << center.z << std::endl;
}

void rotateSceneRight() {
	rotate_scene += 1.f;
}

void rotateCameraDown() {
	pitch -= 0.001f;
	glm::vec3 up = glm::vec3(upVector.x, upVector.y, upVector.z);
	cameraDir = (cameraDir * cos(0.001f) - (up * cos(0.001f)));
	glm::vec3 rightVector = Math::cross3(cameraDir, up);

	glm::vec3 center = glm::vec3(eyePosition.x, eyePosition.y, eyePosition.z);
	center += cameraDir;
	centerPosition = Vector3D(center.x, center.y, center.z);
}

void drawDotProduct(Vector3D a, Vector3D b){
	glLineWidth(2.5);
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(a.x, a.y, 0);
	glEnd();

	glLineWidth(2.5);
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(b.x, b.y, 0);
	glEnd();

	float dot = Math::dot(a, b);

	glLineWidth(5);
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(dot, b.y, 0);
	glEnd();
}

void drawLine(Vector3D a, Vector3D b) {
	glLineWidth(2.5);
	glColor3f(0.3, 0.3, 0.3);
	glBegin(GL_LINES);
	glVertex3f(a.x, a.y, a.z);
	glVertex3f(b.x, b.y, b.z);
	glEnd();
}

void drawRectangle(int x, int y, int w, int h) {
	glBegin(GL_QUADS);
	glVertex2f(x, y);
	glVertex2f(x + w, y);
	glVertex2f(x + w, y + h);
	glVertex2f(x, y + h);
	glEnd();
}

void drawCrossProduct(Vector3D a, Vector3D b){

	glLineWidth(2.5);
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(a.x, a.y, a.z);
	glEnd();

	glLineWidth(2.5);
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(b.x, b.y, b.z);
	glEnd();

	Vector3D cross = Math::cross(a, b);

	glLineWidth(2.5);
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(cross.x, cross.y, cross.z);
	glEnd();
}

void stop() {
	if (Math::abs(speed) < .1f) {
		speed = 0.f;
	}
	else if (speed > 0.f) {
		speed -= .05f;
	}
	else {
		speed += .05f;
	}

	scale_op = 0.f;
}

void moveVertices(unsigned char key) {
	Mesh mesh = meshes[ig.selectedMeshIndex];
	if (key == 'w') {
		for (int i = 0; i < meshes[ig.selectedMeshIndex].vertices.size(); i++) {
			if (meshes[ig.selectedMeshIndex].vertices[i].selected == true) {
				meshes[ig.selectedMeshIndex].vertices[i].Position.z += 1.f;
			}
		}
	}
	else if (key == 's') {
		for (int i = 0; i < meshes[ig.selectedMeshIndex].vertices.size(); i++) {
			if (meshes[ig.selectedMeshIndex].vertices[i].selected == true) {
				meshes[ig.selectedMeshIndex].vertices[i].Position.z -= 1.f;
			}
		}
	}
	if (key == 'a') {
		for (int i = 0; i < meshes[ig.selectedMeshIndex].vertices.size(); i++) {
			if (meshes[ig.selectedMeshIndex].vertices[i].selected == true) {
				meshes[ig.selectedMeshIndex].vertices[i].Position.x -= 1.f;
			}
		}
	}
	else if (key == 'd') {
		for (int i = 0; i < meshes[ig.selectedMeshIndex].vertices.size(); i++) {
			if (meshes[ig.selectedMeshIndex].vertices[i].selected == true) {
				meshes[ig.selectedMeshIndex].vertices[i].Position.x += 1.f;
			}
		}
	}
	if (key == 'q') {
		for (int i = 0; i < meshes[ig.selectedMeshIndex].vertices.size(); i++) {
			if (meshes[ig.selectedMeshIndex].vertices[i].selected == true) {
				meshes[ig.selectedMeshIndex].vertices[i].Position.y -= 1.f;
			}
		}
	}
	if (key == 'e') {
		for (int i = 0; i < meshes[ig.selectedMeshIndex].vertices.size(); i++) {
			if (meshes[ig.selectedMeshIndex].vertices[i].selected == true) {
				meshes[ig.selectedMeshIndex].vertices[i].Position.y += 1.f;
			}
		}
	}
	//mesh.setupMesh();
}

void keyboard_down(unsigned char key, int x, int y) {

}

void keyboard_up(unsigned char key, int x, int y) {

}

void special_keyboard_down(int key, int x, int y) {

}

void special_keyboard_up(int key, int x, int y) {

}

void mouseWheel(int wheel, int direction, int x, int y)
{
	wheel = 0;
	if (direction == -1)
	{
		zoom -= ZOOM_SPEED;

	}
	else if (direction == +1)
	{
		zoom += ZOOM_SPEED;
	}

	glutPostRedisplay();

}

void dragEventHandler(int x, int y) {
	mouse_x = x;
	mouse_y = y - 150;

	glm::mat4 projection2 = getCurrentProjection();
	glm::mat4 ortho2 = glm::ortho(0.f, (float)scene_w, (float)scene_h, 0.f);
	glm::mat4 view2 = getCurrentView();
	glm::mat4 model2 = glm::mat4(1.0f);
	model2 = glm::scale(model2, glm::vec3(zoom, zoom, zoom));
	glm::mat4 mv = view2 * model2;
	glm::mat4 p = projection2;

	glm::vec3 mouse_world_nearplane = glm::unProject(
		glm::vec3(mouse_x, scene_h - mouse_y, 0.0f),
		mv,
		p,
		glm::vec4(0, 0, scene_w, scene_h));

	glm::vec3 mouse_world_farplane = glm::unProject(
		glm::vec3(mouse_x, scene_h - mouse_y, 1.0f),
		mv,
		p,
		glm::vec4(0, 0, scene_w, scene_h));

	for (int i = 0; i < meshes.size(); i++) {
		for (int j = 0; j < meshes[i].vertices.size(); j++) {
			glm::vec3 closest_point = glm::closestPointOnLine(
				meshes[i].vertices[j].Position,
				mouse_world_nearplane,
				mouse_world_farplane);
			if (glm::distance(meshes[i].vertices[j].Position, closest_point) < 5.f && meshes[i].vertices[j].selected == true) {
				glm::vec3 delta = Math::subtract2(closest_point, meshes[i].vertices[j].Position);
				//std::cout << "x = " << delta.x << ", y = " << delta.y << ", z = " << delta.z << std::endl;
				meshes[i].vertices[j].Position = Math::add2(meshes[i].vertices[j].Position, delta);
				//std::cout << "dragging" << std::endl;
			}
		}
	}
}

void drawExportOBJ() {
	if (ig.export_active == true) {
		ImGui::SetNextWindowPos(ImVec2(0, 150));
		ImGui::SetNextWindowSize(ImVec2(500, 400));
		if (ImGuiFileDialog::Instance()->FileDialog("Choose File", ".obj\0\0\0", ".", ""))
		{
			if (ImGuiFileDialog::Instance()->IsOk == true)
			{
				string filePathName = ImGuiFileDialog::Instance()->GetFilepathName();
				string path = ImGuiFileDialog::Instance()->GetCurrentPath();
				string fileName = ImGuiFileDialog::Instance()->GetCurrentFileName();
				string ext = ImGuiFileDialog::Instance()->GetCurrentFilter();

				//std::cout << filePathName + ext << std::endl;

				ofstream myfile;
				myfile.open(filePathName + ext, ios::out | ios::app | ios::binary);
				if (myfile.is_open())
				{
					for (int i = 0; i < meshes.size(); i++) {
						for (int j = 0; j < meshes[i].vertices.size(); j++) {
							glm::vec3 vert = meshes[i].vertices[j].Position;
							myfile << "v " << vert.x << " " << vert.y << " " << vert.z << std::endl;
						}
					}

					for (int i = 0; i < meshes.size(); i++) {
						for (int j = 0; j < meshes[i].indices.size(); j += 3) {
							unsigned int ind1 = meshes[i].indices[j];
							unsigned int ind2 = meshes[i].indices[j + 1];
							unsigned int ind3 = meshes[i].indices[j + 2];
							myfile << "f " << ind1 << " " << ind2 << " " << ind3 << std::endl;
						}
					}
					myfile.close();
				}
				else {
					std::cout << "Unable to create file" << std::endl;
				}
			}
			else
			{
				std::cout << "No file selected." << std::endl;
			}
			ig.export_active = false;
		}
	}
	
}

void drawSaveRender() {
	if (ig.save_render_active == true) {
		ImGui::SetNextWindowPos(ImVec2(0, 150));
		ImGui::SetNextWindowSize(ImVec2(500, 400));
		if (ImGuiFileDialog::Instance()->FileDialog("Choose File", ".jpg\0\0\0", ".", ""))
		{
			if (ImGuiFileDialog::Instance()->IsOk == true)
			{
				string filePathName = ImGuiFileDialog::Instance()->GetFilepathName();
				string path = ImGuiFileDialog::Instance()->GetCurrentPath();
				string fileName = ImGuiFileDialog::Instance()->GetCurrentFileName();
				string ext = ImGuiFileDialog::Instance()->GetCurrentFilter();

				int width, height, channels;

				string file1 = filePathName + ext;

				vector< unsigned char > data(scene_w * scene_h * 3);

				glMatrixMode(GL_TEXTURE);
				glBindTexture(GL_TEXTURE_2D, tex);
				glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, &data[0]);

				stbi_flip_vertically_on_write(1);
				int saved = stbi_write_jpg(file1.c_str(), scene_w, scene_h, 3, &data[0], 100);
			}
			else
			{
				std::cout << "No file selected." << std::endl;
			}
			ig.save_render_active = false;
		}
	}
}

void mouseEventHandler(int button, int state, int x, int y) {
	
	switch (button) {
		case GLUT_LEFT_BUTTON:

			if (state == GLUT_DOWN) {
				//std::cout << "x = " << x << ", y = " << y << std::endl;
				mouse_x = x;
				mouse_y = y - 150;
				mouse_down = true;

				glm::mat4 projection2 = getCurrentProjection();
				glm::mat4 ortho2 = glm::ortho(0.f, (float)scene_w, (float)scene_h, 0.f);
				glm::mat4 view2 = getCurrentView();
				glm::mat4 model2 = glm::mat4(1.0f);
				model2 = glm::scale(model2, glm::vec3(zoom, zoom, zoom));
				glm::mat4 mv = view2 * model2;
				glm::mat4 p = projection2;

				glm::vec3 mouse_world_nearplane = glm::unProject(
					glm::vec3(mouse_x, scene_h - mouse_y, 0.0f),
					mv,
					p,
					glm::vec4(0, 0, scene_w, scene_h));

				glm::vec3 mouse_world_farplane = glm::unProject(
					glm::vec3(mouse_x, scene_h - mouse_y, 1.0f),
					mv,
					p,
					glm::vec4(0, 0, scene_w, scene_h));

				for (int i = 0; i < meshes.size(); i++) {
					for (int j = 0; j < meshes[i].vertices.size(); j++) {
						glm::vec3 closest_point = glm::closestPointOnLine(
							meshes[i].vertices[j].Position,
							mouse_world_nearplane,
							mouse_world_farplane);
						
						if (glm::distance(meshes[i].vertices[j].Position, closest_point) < 5.f) {
							if (meshes[i].vertices[j].selected == false) {
								meshes[i].vertices[j].selected = true;
							}
							else {
								meshes[i].vertices[j].selected = false;
							}
							//std::cout << "Vertice nr. " << j << " selected." << std::endl;
						}
					}
				}
			}
			else if (state == GLUT_UP) {
				mouse_down = false;
			}
			break;
	}
}

Vector3D ScreenToWorld(int x, int y)
{
	GLint viewport[4];
	GLdouble modelview[16];
	GLdouble projection[16];
	GLfloat winX, winY, winZ;
	GLdouble posX, posY, posZ;

	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	glGetDoublev(GL_PROJECTION_MATRIX, projection);
	glGetIntegerv(GL_VIEWPORT, viewport);

	winX = (float)x;
	winY = (float)viewport[3] - (float)y;
	glReadPixels(x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

	gluUnProject(winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

	return Vector3D(posX, posY, posZ);
}

void drawToolbar(ImGuiIO& io) {
	ImGui::SetNextWindowPos(ImVec2(0, 0));
	ImGui::SetNextWindowSize(ImVec2(scene_w, 150));

	ImGui::Begin("Toolbar", &ig.toolbar_active, ImGuiWindowFlags_MenuBar);
	if (ImGui::BeginMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			if (ImGui::MenuItem("Open..", "Ctrl+O")) { /* TO_DO */ }
			if (ImGui::MenuItem("Save", "Ctrl+S")) { /* TO_DO */ }
			if (ImGui::MenuItem("Close", "Ctrl+W")) { ig.toolbar_active = false; }
			ImGui::EndMenu();
		}
		ImGui::EndMenuBar();
	}


	if (ImGui::Button("Box")) {
		ig.box_active = true;
		ig.cylinder_active = false;
		ig.plane_active = false;
		ig.sphere_active = false;
		ig.model_active = false;
	}
	ImGui::SameLine();
	if (ImGui::Button("Cylinder")) {
		ig.box_active = false;
		ig.cylinder_active = true;
		ig.plane_active = false;
		ig.sphere_active = false;
		ig.model_active = false;
	}
	ImGui::SameLine();
	if (ImGui::Button("Plane")) {
		ig.box_active = false;
		ig.cylinder_active = false;
		ig.plane_active = true;
		ig.sphere_active = false;
		ig.model_active = false;
	}
	ImGui::SameLine();
	if (ImGui::Button("Sphere")) {
		ig.box_active = false;
		ig.cylinder_active = false;
		ig.plane_active = false;
		ig.sphere_active = true;
		ig.model_active = false;
	}
	ImGui::SameLine();
	if (ImGui::Button("Model")) {
		ig.box_active = false;
		ig.cylinder_active = false;
		ig.plane_active = false;
		ig.sphere_active = false;
		ig.model_active = true;
	}
	ImGui::SameLine();
	if (ImGui::Button("Image")) {
		ig.image_dialog_active = true;
		ig.save_render_active = false;
		ig.export_active = false;
		ig.model_active = false;
	}
	ImGui::SameLine();
	if (ImGui::Button("Export")) {
		ig.export_active = true;
		ig.model_active = false;
		ig.save_render_active = false;
		ig.image_dialog_active = false;
	}
	ImGui::SameLine();
	if (ImGui::Button("Render")) {
		ig.save_render_active = true;
		ig.export_active = false;
		ig.model_active = false;
		ig.image_dialog_active = false;
	}
	if (ImGui::Button("Left")) {
		ig.view = "left";
	}
	ImGui::SameLine();
	if (ImGui::Button("Front")) {
		ig.view = "front";
	}
	ImGui::SameLine();
	if (ImGui::Button("Right")) {
		ig.view = "right";
	}
	ImGui::SameLine();
	if (ImGui::Button("Back")) {
		ig.view = "back";
	}
	ImGui::SameLine();
	if (ImGui::Button("Top")) {
		ig.view = "top";
	}
	ImGui::SameLine();
	if (ImGui::Button("Bottom")) {
		ig.view = "bottom";
	}
	ImGui::SameLine();
	if (ImGui::Button("Ortho")) {
		ig.projection = "ortho";
		ig.view = "ortho";
	}
	ImGui::SameLine();
	if (ImGui::Button("Perspective")) {
		ig.projection = "perspective";
		ig.view = "perspective";
	}
	
	/*ImGui::Text("Keys down:");*/      
	for (int i = 0; i < IM_ARRAYSIZE(io.KeysDown); i++) if (io.KeysDownDuration[i] >= 0.0f) { 
		/*ImGui::SameLine(); ImGui::Text("%d (0x%X) (%.02f secs)", i, i, io.KeysDownDuration[i]);*/ 
		if (i == 87) {
			moveVertices('w');
		}
		if (i == 83) {
			moveVertices('s');
		}
		if (i == 65) {
			moveVertices('a');
		}
		if (i == 68) {
			moveVertices('d');
		}
		if (i == 357) {
			rotateCameraUp();
		}
		if (i == 356) {
			rotateSceneLeft();
		}
		if (i == 359) {
			rotateCameraDown();
		}
		if (i == 358) {
			rotateSceneRight();
		}
		if (i == 81) {
			moveVertices('q');
		}
		if (i == 69) {
			moveVertices('e');
		}
	}
	/*ImGui::Text("Keys pressed:");   for (int i = 0; i < IM_ARRAYSIZE(io.KeysDown); i++) if (ImGui::IsKeyPressed(i)) { /*ImGui::SameLine(); ImGui::Text("%d (0x%X)", i, i); }
	ImGui::Text("Keys release:");   for (int i = 0; i < IM_ARRAYSIZE(io.KeysDown); i++) if (ImGui::IsKeyReleased(i)) { ImGui::SameLine(); ImGui::Text("%d (0x%X)", i, i); }
	ImGui::Text("Keys mods: %s%s%s%s", io.KeyCtrl ? "CTRL " : "", io.KeyShift ? "SHIFT " : "", io.KeyAlt ? "ALT " : "", io.KeySuper ? "SUPER " : "");
	ImGui::Text("Chars queue:");    for (int i = 0; i < io.InputQueueCharacters.Size; i++) { ImWchar c = io.InputQueueCharacters[i]; ImGui::SameLine();  ImGui::Text("\'%c\' (0x%04X)", (c > ' ' && c <= 255) ? (char)c : '?', c); }*/
	ImGui::End();
}

void drawImageDialog() {
	if (ig.image_dialog_active) {
		ImGui::SetNextWindowPos(ImVec2(0, 150));
		ImGui::SetNextWindowSize(ImVec2(500, 400));
		if (ImGuiFileDialog::Instance()->FileDialog("Choose File", ".jpg\0\0", ".", ""))
		{
			if (ImGuiFileDialog::Instance()->IsOk == true)
			{
				string filePathName = ImGuiFileDialog::Instance()->GetFilepathName();
				string path = ImGuiFileDialog::Instance()->GetCurrentPath();
				string fileName = ImGuiFileDialog::Instance()->GetCurrentFileName();
				string ext = ImGuiFileDialog::Instance()->GetCurrentFilter();

				Texture img = Texture(shader2DSimple);
				img.Load(filePathName.c_str());
				images.push_back(img);
			}
			else {
				std::cout << "No file selected." << std::endl;
			}
			ig.image_dialog_active = false;
		}
	}
}

void drawColorPicker() {
	if (ig.color_active) {
		ImGui::SetNextWindowPos(ImVec2(window_w - 450, 150));
		ImGui::SetNextWindowSize(ImVec2(150, 300));
		ImGui::Begin("Color");
		ImGui::ColorPicker4("", (float*)& ig.properties.color, ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_NoSmallPreview);
		ImGui::End();
	}
}

void drawBoxCreateDialog() {
	if (ig.box_active) {
		ImGui::SetNextWindowPos(ImVec2(0, 150));
		ImGui::SetNextWindowSize(ImVec2(150, 300));
		ImGui::Begin("Box", &ig.box_active);

		if (ImGui::InputText("X", ig.box_x, IM_ARRAYSIZE(ig.box_x), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.box_x;
		}
		if (ImGui::InputText("Y", ig.box_y, IM_ARRAYSIZE(ig.box_y), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.box_y;
		}
		if (ImGui::InputText("Z", ig.box_z, IM_ARRAYSIZE(ig.box_z), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.box_z;
		}
		if (ImGui::Button("Create")) {
			Box box1(50.0, 50.0, 50.0);
			box1.setPosition(Vector3D(atof(ig.box_x), atof(ig.box_y), atof(ig.box_z)));
			box1.setColor({ 0.5f, 0.5f, 0.5f });
			vector<Texture> textures = { };
			Mesh mesh1 = Mesh();
			mesh1.Load(box1.GetVertices(), box1.GetIndices(), box1.GetIndicesQuads(), textures, shader1, 4);
			//std::cout << "Nr. of edges: " << mesh1.GetEdges().size() << std::endl;
			meshes.push_back(mesh1);
		}
		ImGui::End();
	}
}

void drawProperties() {
	ImGui::SetNextWindowPos(ImVec2(window_w - 300, 450));
	ImGui::SetNextWindowSize(ImVec2(300, 500));
	ImGui::Begin("Properties");
	ImGui::Text("Position");
	ImGui::PushItemWidth(50);
	if (ImGui::InputTextWithHint("##X", "X", ig.properties.x, IM_ARRAYSIZE(ig.properties.x), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.x;
	}
	ImGui::SameLine();
	if (ImGui::InputTextWithHint("##Y", "Y", ig.properties.y, IM_ARRAYSIZE(ig.properties.y), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.y;
	}
	ImGui::SameLine();
	if (ImGui::InputTextWithHint("##Z", "Z", ig.properties.z, IM_ARRAYSIZE(ig.properties.z), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.z;
	}
	ImGui::Text("Rotation");
	if (ImGui::InputTextWithHint("##X2", "X", ig.properties.rotate_x, IM_ARRAYSIZE(ig.properties.rotate_x), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.rotate_x;
	}
	ImGui::SameLine();
	if (ImGui::InputTextWithHint("##Y2", "Y", ig.properties.rotate_y, IM_ARRAYSIZE(ig.properties.rotate_y), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.rotate_y;
	}
	ImGui::SameLine();
	if (ImGui::InputTextWithHint("##Z2", "Z", ig.properties.rotate_z, IM_ARRAYSIZE(ig.properties.rotate_z), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.rotate_z;
	}
	ImGui::Text("Scale");
	if (ImGui::InputTextWithHint("##X3", "X", ig.properties.scale_x, IM_ARRAYSIZE(ig.properties.scale_x), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.scale_x;
	}
	ImGui::SameLine();
	if (ImGui::InputTextWithHint("##Y3", "Y", ig.properties.scale_y, IM_ARRAYSIZE(ig.properties.scale_y), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.scale_y;
	}
	ImGui::SameLine();
	if (ImGui::InputTextWithHint("##Z3", "Z", ig.properties.scale_z, IM_ARRAYSIZE(ig.properties.scale_z), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.scale_z;
	}
	ImGui::PushItemWidth(150);
	ImGui::Text("Texture");
	if (ImGui::InputText("##texture_path", ig.properties.texture_path, IM_ARRAYSIZE(ig.properties.texture_path), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.texture_path;
	}
	if (ImGui::InputTextWithHint("##X4", "X", ig.properties.texcoords_x, IM_ARRAYSIZE(ig.properties.texcoords_x), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.texcoords_x;
	}
	ImGui::SameLine();
	if (ImGui::InputTextWithHint("##Y4", "Y", ig.properties.texcoords_y, IM_ARRAYSIZE(ig.properties.texcoords_y), ImGuiInputTextFlags_EnterReturnsTrue)) {
		char* s = ig.properties.texcoords_y;
	}
	if (ImGui::Button("Repeat")) {
		meshes[ig.selectedMeshIndex].SetTexParam(1, GL_REPEAT);
	}
	ImGui::SameLine();
	if (ImGui::Button("Clamp")) {
		meshes[ig.selectedMeshIndex].SetTexParam(1, GL_CLAMP);
	}
	if (ImGui::Button("Color")) {
		ig.color_active = true;
		meshes[ig.selectedMeshIndex].texture_type = "color";
	}
	ImGui::SameLine();
	if (ImGui::Button("Image")) {
		meshes[ig.selectedMeshIndex].texture_type = "image";
	}
	if (ImGui::Button("Remove")) {
		meshes.erase(meshes.begin() + ig.selectedMeshIndex);
	}
	if (ImGui::Button("Subdivide")) {
		meshes[ig.selectedMeshIndex].Subdivide();
	}
	if (ImGui::Button("Vertex Mode")) {
		if (ig.vertex_mode == false) {
			ig.vertex_mode = true;
		}
		else {
			ig.vertex_mode = false;
		}
	}
	if (ImGui::Button("Update")) {
		glm::vec3 pos = glm::vec3((GLfloat)atof(ig.properties.x), (GLfloat)atof(ig.properties.y), (GLfloat)atof(ig.properties.z));
		if (pos.x != meshes[ig.selectedMeshIndex].position.x || pos.y != meshes[ig.selectedMeshIndex].position.y || pos.z != meshes[ig.selectedMeshIndex].position.z) {
			meshes[ig.selectedMeshIndex].SetPosition(pos);
		}
		vector<float> col = { ig.properties.color[0], ig.properties.color[1], ig.properties.color[2] };
		meshes[ig.selectedMeshIndex].rotateVerticesX(atof(ig.properties.rotate_x));
		meshes[ig.selectedMeshIndex].rotateVerticesY(atof(ig.properties.rotate_y));
		meshes[ig.selectedMeshIndex].rotateVerticesZ(atof(ig.properties.rotate_z));
		meshes[ig.selectedMeshIndex].SetScaleX(atof(ig.properties.scale_x));
		meshes[ig.selectedMeshIndex].SetScaleY(atof(ig.properties.scale_y));
		meshes[ig.selectedMeshIndex].SetScaleZ(atof(ig.properties.scale_z));
		meshes[ig.selectedMeshIndex].SetScale();
		meshes[ig.selectedMeshIndex].SetTexCoords(glm::vec2(atof(ig.properties.texcoords_x), atof(ig.properties.texcoords_y)));
		if (meshes[ig.selectedMeshIndex].texture_type == "image") {
			meshes[ig.selectedMeshIndex].SetDefaultTexture((char*)ig.properties.texture_path);
		}
		else {
			meshes[ig.selectedMeshIndex].SetColor(col);
		}
		memset(ig.properties.x, 0, sizeof(ig.properties.x));
		memset(ig.properties.y, 0, sizeof(ig.properties.y));
		memset(ig.properties.z, 0, sizeof(ig.properties.z));
		memset(ig.properties.rotate_x, 0, sizeof(ig.properties.rotate_x));
		memset(ig.properties.rotate_y, 0, sizeof(ig.properties.rotate_y));
		memset(ig.properties.rotate_z, 0, sizeof(ig.properties.rotate_z));
		memset(ig.properties.scale_x, 0, sizeof(ig.properties.scale_x));
		memset(ig.properties.scale_y, 0, sizeof(ig.properties.scale_y));
		memset(ig.properties.scale_z, 0, sizeof(ig.properties.scale_z));
		ig.properties.scale_x[0] = '1';
		ig.properties.scale_y[0] = '1';
		ig.properties.scale_z[0] = '1';
		ig.properties.rotate_x[0] = '0';
		ig.properties.rotate_y[0] = '0';
		ig.properties.rotate_z[0] = '0';
		strcpy(ig.properties.texcoords_x, std::to_string(meshes[ig.selectedMeshIndex].TexCoords.x).c_str());
		strcpy(ig.properties.texcoords_y, std::to_string(meshes[ig.selectedMeshIndex].TexCoords.y).c_str());
		strcpy(ig.properties.x, std::to_string((int)meshes[ig.selectedMeshIndex].position.x).c_str());
		strcpy(ig.properties.y, std::to_string((int)meshes[ig.selectedMeshIndex].position.y).c_str());
		strcpy(ig.properties.z, std::to_string((int)meshes[ig.selectedMeshIndex].position.z).c_str());
	}
	ImGui::End();
}

void drawSceneTree() {
	ImGui::SetNextWindowPos(ImVec2(window_w - 300, 150));
	ImGui::SetNextWindowSize(ImVec2(300, 300));
	ImGui::Begin("Scene");

	std::string s = "Scene";
	const char* s2 = s.c_str();

	vector<string> objects = {};

	for (int i = 0; i < meshes.size(); i++) {
		std::string s = std::to_string(i);
		objects.push_back("Object" + s);
	}
	
	if (ImGui::ListBoxHeader("", ig.selectedMeshIndex, 6))
	{
		for (int i = 0; i < objects.size(); i++) {
			bool selected = false;
			if (i == ig.selectedMeshIndex) {
				selected = true;
			}
			if (selected == true && meshes[i].selected == false) {
				meshes[i].selected = true;
				strcpy(ig.properties.texture_path, meshes[i].texture_path);
				strcpy(ig.properties.texcoords_x, std::to_string(meshes[i].TexCoords.x).c_str());
				strcpy(ig.properties.texcoords_y, std::to_string(meshes[i].TexCoords.y).c_str());
				strcpy(ig.properties.x, std::to_string((int)meshes[i].position.x).c_str());
				strcpy(ig.properties.y, std::to_string((int)meshes[i].position.y).c_str());
				strcpy(ig.properties.z, std::to_string((int)meshes[i].position.z).c_str());
			}
			if (selected == false) {
				meshes[i].selected = false;
			}
			if (ImGui::Selectable(objects[i].c_str(), selected)) {
				ig.selectedMeshIndex = i;
			}
		}

		
		ImGui::ListBoxFooter();
	}
	

	ImGui::End();
}

void drawCylinderCreateDialog() {
	if (ig.cylinder_active) {
		ImGui::SetNextWindowPos(ImVec2(0, 150));
		ImGui::SetNextWindowSize(ImVec2(150, 300));
		ImGui::Begin("Cylinder", &ig.cylinder_active);

		if (ImGui::InputText("X", ig.cylinder_x, IM_ARRAYSIZE(ig.cylinder_x), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.cylinder_x;
		}
		if (ImGui::InputText("Y", ig.cylinder_y, IM_ARRAYSIZE(ig.cylinder_y), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.cylinder_y;
		}
		if (ImGui::InputText("Z", ig.cylinder_z, IM_ARRAYSIZE(ig.cylinder_z), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.cylinder_z;
		}
		if (ImGui::InputText("Base", ig.base, IM_ARRAYSIZE(ig.base), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.base;
		}
		if (ImGui::InputText("Top", ig.top, IM_ARRAYSIZE(ig.top), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.top;
		}
		if (ImGui::InputText("Height", ig.height, IM_ARRAYSIZE(ig.height), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.height;
		}
		if (ImGui::Button("Create")) {
			Cylinder cylinder1(atoi(ig.base), atoi(ig.top), atoi(ig.height), 50.0f, 50.0f);
			cylinder1.setPosition(Vector3D(atoi(ig.cylinder_x), atoi(ig.cylinder_y), atoi(ig.cylinder_z)));
			cylinder1.setColor({ 0.5f, 0.5f, 0.5f });
			vector<Texture> textures = { };
			Mesh mesh1 = Mesh();
			mesh1.Load(cylinder1.GetVertices(), cylinder1.GetIndices(), {}, textures, shader1, 3);
			meshes.push_back(mesh1);
		}
		ImGui::End();
	}
}

void drawPlaneCreateDialog() {
	if (ig.plane_active) {
		ImGui::SetNextWindowPos(ImVec2(0, 150));
		ImGui::SetNextWindowSize(ImVec2(150, 300));
		ImGui::Begin("Plane", &ig.plane_active);

		if (ImGui::InputText("X", ig.plane_x, IM_ARRAYSIZE(ig.plane_x), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.plane_x;
		}
		if (ImGui::InputText("Y", ig.plane_y, IM_ARRAYSIZE(ig.plane_y), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.plane_y;
		}
		if (ImGui::InputText("Z", ig.plane_z, IM_ARRAYSIZE(ig.plane_z), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.plane_z;
		}
		if (ImGui::Button("Create")) {
			Plane plane1(25.0f, 25.0f);
			plane1.setPosition(Vector3D(atof(ig.plane_x), atof(ig.plane_y), atof(ig.plane_z)));
			plane1.setColor({ 0.5f, 0.5f, 0.5f });
			vector<Texture> textures = { };
			Mesh mesh1 = Mesh();
			mesh1.Load(plane1.GetVertices(), plane1.GetIndices(), plane1.GetIndicesQuads(), textures, shader1, 4);
			meshes.push_back(mesh1);
		}
		ImGui::End();
	}
}

void drawSphereCreateDialog() {
	if (ig.sphere_active) {
		ImGui::SetNextWindowPos(ImVec2(0, 150));
		ImGui::SetNextWindowSize(ImVec2(150, 300));
		ImGui::Begin("Sphere", &ig.sphere_active);

		if (ImGui::InputText("X", ig.sphere_x, IM_ARRAYSIZE(ig.sphere_x), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.sphere_x;
		}
		if (ImGui::InputText("Y", ig.sphere_y, IM_ARRAYSIZE(ig.sphere_y), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.sphere_y;
		}
		if (ImGui::InputText("Z", ig.sphere_z, IM_ARRAYSIZE(ig.sphere_z), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.sphere_z;
		}
		if (ImGui::InputText("Radius", ig.sphere_r, IM_ARRAYSIZE(ig.sphere_z), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.sphere_r;
		}
		if (ImGui::InputText("Slices", ig.sphere_slices, IM_ARRAYSIZE(ig.sphere_slices), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.sphere_slices;
		}
		if (ImGui::InputText("Stacks", ig.sphere_stacks, IM_ARRAYSIZE(ig.sphere_stacks), ImGuiInputTextFlags_EnterReturnsTrue)) {
			char* s = ig.sphere_stacks;
		}
		if (ImGui::Button("Create")) {
			Sphere sphere1(atof(ig.sphere_r), atoi(ig.sphere_slices), atoi(ig.sphere_stacks));
			sphere1.setPosition(Vector3D(atoi(ig.sphere_x), atoi(ig.sphere_y), atoi(ig.sphere_z)));
			sphere1.setColor({ 0.5f, 0.5f, 0.5f });
			vector<Texture> textures = { };
			Mesh mesh1 = Mesh();
			mesh1.Load(sphere1.GetVertices(), sphere1.GetIndices(), {}, textures, shader1, 3);
			meshes.push_back(mesh1);
		}
		ImGui::End();
	}
}

void captureInput(ImGuiIO& io) {
	if (ImGui::IsMouseClicked(0)) {
		mouseEventHandler(GLUT_LEFT_BUTTON, GLUT_DOWN, io.MousePos.x, io.MousePos.y);
	}
	if (ImGui::IsMouseReleased(0)) {
		mouseEventHandler(GLUT_LEFT_BUTTON, GLUT_UP, io.MousePos.x, io.MousePos.y);
	}
	if (ImGui::IsMouseDragging(0)) {
		dragEventHandler(io.MousePos.x, io.MousePos.y);
	}
	if (io.MouseWheel > 0) {
		mouseWheel(io.MouseWheel, 1, 0, 0);
	}
	else if (io.MouseWheel < 0) {
		mouseWheel(io.MouseWheel, -1, 0, 0);
	}
}

void drawPreviewWindow() {
	ImGui::SetNextWindowPos(ImVec2(0, 150));
	ImGui::SetNextWindowSize(ImVec2(scene_w, scene_h));
	bool preview_active = true;
	ImGui::Begin("Preview", &preview_active, ImGuiWindowFlags_NoBringToFrontOnFocus);

	// Ask ImGui to draw texture as an image:
	// Under OpenGL the ImGUI image type is GLuint
	// Make sure to use "(void *)tex" but not "&tex"
	ImGui::GetWindowDrawList()->AddImage((void*)tex, ImVec2(ImGui::GetItemRectMin().x, 150), ImVec2(scene_w, scene_h + 150), ImVec2(0, 1), ImVec2(1, 0));
	ImGui::End();
}

void drawImportModel() {
	if (ig.model_active) {
		ImGui::SetNextWindowPos(ImVec2(0, 150));
		ImGui::SetNextWindowSize(ImVec2(500, 400));
		if (ImGuiFileDialog::Instance()->FileDialog("Choose File", ".obj\0.blend\0\0", ".", ""))
		{
			if (ImGuiFileDialog::Instance()->IsOk == true)
			{
				string filePathName = ImGuiFileDialog::Instance()->GetFilepathName();
				string path = ImGuiFileDialog::Instance()->GetCurrentPath();
				string fileName = ImGuiFileDialog::Instance()->GetCurrentFileName();
				
				Model model1(filePathName, path, shader1);
				model1.SetPosition(glm::vec3(0.f, 0.f, 0.f));
				models.push_back(model1);
				for (int i = 0; i < model1.meshes.size(); i++) {
					meshes.push_back(model1.meshes[i]);
				}
			}
			else
			{
				std::cout << "No file selected." << std::endl;
			}
			ig.model_active = false;
		}
	}
}

void drawGUI() {
	ImGuiIO& io = ImGui::GetIO();
	ImGui_ImplOpenGL2_NewFrame();
	ImGui_ImplGLUT_NewFrame();

	captureInput(io);

	drawToolbar(io);
	drawSceneTree();
	drawProperties();
	drawColorPicker();
	drawBoxCreateDialog();
	drawCylinderCreateDialog();
	drawPlaneCreateDialog();
	drawSphereCreateDialog();
	drawImageDialog();
	drawImportModel();
	drawExportOBJ();
	drawSaveRender();
	drawPreviewWindow();
	
	
	ImGui::Render();

	ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());
}

void renderQuad() {
	//Render quad
	glBegin(GL_QUADS);
	glVertex2f(-50.f, -50.f);
	glVertex2f(50.f, -50.f);
	glVertex2f(50.f, 50.f);
	glVertex2f(-50.f, 50.f);
	glEnd();
}



void render() {
	glClearColor(0.8f, 0.8f, 0.8f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, scene_w, scene_h);
	/*
	glShadeModel(GL_SMOOTH);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	*/
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	for (int i = 0; i < images.size(); i++) {
		int w = images[i].textureWidth();
		int h = images[i].textureHeight();
		images[i].renderQuad(-(w/2), -(h/2), scene_w, scene_h);
	}


	shader1.use();

	
	glm::mat4 projection = getCurrentProjection();
	glm::mat4 ortho = glm::ortho(0.f, (float)scene_w, (float)scene_h, 0.f);
	glm::mat4 view = getCurrentView();
	shader1.setMat4("projection", projection);
	shader1.setMat4("view", view);

	// render the loaded model
	glm::mat4 model = glm::mat4(1.0f);
	//model = glm::translate(model, glm::vec3(1.0f, 0.0, 0.0f));
	model = glm::scale(model, glm::vec3(zoom, zoom, zoom));	// it's a bit too big for our scene, so scale it down
	//model = glm::rotate(model, PI / 2, glm::vec3(1.f, 0.f, 0.f));
	shader1.setMat4("model", model);

	glm::vec3 lightPos = glm::vec3(60.f, 60.f, 60.f);
	shader1.setVec3("lightPos", lightPos);
	shader1.setVec3("viewPos", glm::vec3(eyePosition.x, eyePosition.y, eyePosition.z));
	shader1.setVec3("lightColor", glm::vec3(1.f, 1.f, 1.f));

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	drawLine(Vector3D(0, 0, 0), Vector3D(5.0, 0.0, 0.0));

	drawLine(Vector3D(0, 0, 0), Vector3D(0.0, 5.0, 0.0));

	drawLine(Vector3D(0, 0, 0), Vector3D(0.0, 0.0, 5.0));

	for (int i = 0; i < meshes.size(); i++) {
		meshes[i].Draw();
	}

	shader1.active(false);
	glBindVertexArray(0);
	glDisable(GL_CULL_FACE);
	if (ig.vertex_mode == true) {
		glm::vec3 color2 = glm::vec3(1.0, 0.0, 0.0);
		shader2D.setVec3("color_in", color2);
		shader2D.use();

		shader2D.setMat4("model", model);
		shader2D.setMat4("projection", ortho);
		shader2D.setMat4("view", view);

		glm::mat4 mv = view * model;
		glm::mat4 p = projection;

		for (int i = 0; i < meshes.size(); i++) {
			meshes[i].DrawVertices(mv, p, scene_w, scene_h, shader2D);
		}

		shader2D.active(false);
	}
	
}

void update()
{
	glClearColor(0.8f, 0.8f, 0.8f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	if (gui_active) {
		fbo = 0;
		tex = 0;
		glGenFramebuffers(1, &fbo);
		
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);

		glGenTextures(1, &tex);
		// "Bind" the newly created texture : all future texture functions will modify this texture
		glBindTexture(GL_TEXTURE_2D, tex);
		// Give an empty image to OpenGL ( the last "0" )
		glGetError();
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, scene_w, scene_h, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
		GLenum error = glGetError();
		if (error != GL_NO_ERROR)
		{
			std::cout << "Error: " + std::to_string(error) << std::endl;
		}
		// Poor filtering. Needed !
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		//Bind texture
		glGetError();
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
		
		error = glGetError();
		if (error != GL_NO_ERROR)
		{
			std::cout << "Error: " + std::to_string(error) << std::endl;
		}
		
		/*
		glGenRenderbuffers(1, &rbo);
		glBindRenderbuffer(GL_RENDERBUFFER, rbo);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, scene_w, scene_h);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
		*/
		error = glGetError();
		while (error != GL_NO_ERROR) {
			std::cout << "Error: " + std::to_string(error) << std::endl;
			error = glGetError();
		}
		
		//glClear(GL_DEPTH_BUFFER_BIT);
		
	}

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE) {
		
		//Clear framebuffer
		//glClear(GL_COLOR_BUFFER_BIT);
		//glViewport(0, 0, scene_w, scene_h);
		//Render scene to framebuffer
		render();
		//Unbind framebuffer
		
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		//glBindRenderbuffer(GL_RENDERBUFFER, 0);
		//glDeleteFramebuffers(1, &fbo);
	}
	else {
		std::cout << "Error binding framebuffer: " + std::to_string(glCheckFramebufferStatus(GL_FRAMEBUFFER)) << std::endl;
		glClearTexImage(tex, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	if (gui_active) {
		drawGUI();
	}
	glDeleteFramebuffers(1, &fbo);
	glDeleteRenderbuffers(1, &rbo);
	glDeleteTextures(1, &tex);

	//Update screen
	glFlush();
	glutSwapBuffers();
	glutPostRedisplay();
}

vector<Vector3D> rotateVertices(vector<Vector3D> verts, Vector3D center, float theta, float phi) {
	for (int i = 0; i < verts.size(); i++) {
		verts[i] = Math::rotate2(verts[i], center, theta, phi);
	}

	return verts;
}

void limitSpeed() {

	if (speed < -MAX_SPEED) {
		speed = -MAX_SPEED;
	}
	else if (speed > MAX_SPEED) {
		speed = MAX_SPEED;
	}
	else {

	}
}

//Challenge: Have the camera follow the block in 3rd person.
void moveCamera(Vector3D direction, float speed) {
	Vector3D delta = Math::multiply(direction, speed);
	delta = Math::multiply(delta, zoom);
	centerPosition = Math::add(centerPosition, delta);
	eyePosition = Math::add(eyePosition, delta);
}

void move() {

	limitSpeed();

}

vector<Vector3D> translate(vector<Vector3D> vertices, Vector3D direction, float speed) {
	Vector3D delta = Math::multiply(direction, speed);
	
	for (int i = 0; i < vertices.size(); i++) {
		vertices[i] = Math::add(vertices[i], delta);
	}

	return vertices;
}



void timer(int v)
{
	/*
	if (isMoving) {
		for (int i = 0; i < boxes.size(); i++) {
			if (boxes[i].selected == true) {
				boxes[i].verts = translate(boxes[i].verts, direction, 0.1f);
			}
		}
		moveCamera(direction, 0.1f);
	}

	if (isRotating) {
		for (int i = 0; i < boxes.size(); i++) {
			if (boxes[i].selected == true) {
				boxes[i].verts = rotateVertices(boxes[i].verts, Vector3D(0, 0, 0), rotation_theta, rotation_phi);
			}
		}
	}

	if (isScaling) {
		for (int i = 0; i < boxes.size(); i++) {
			if (boxes[i].selected == true) {
				boxes[i].updateScale(scale_op);
			}
		}
	}
	*/
	//glutPostRedisplay();
	//glutTimerFunc(1000 / 30, timer, v);
}

void mathLibraryTests(){


	Vector3D z = Vector3D(0, 0, 1);
	Vector3D y = Vector3D(0, 1, 0);
	Vector3D x = Math::cross(y, z);
	std::cout << "(0, 0, 1) cross (0, 1, 0) = " << x.x << ", " << x.y << ", " << x.z << std::endl;

	float v = Math::dot(x, y);
	std::cout << "(1, 0, 0) dot (0, 1, 0) = " << v << std::endl;

	Vector3D zParallel = Vector3D(0, 0, 3);
	Vector3D w = Math::cross(zParallel, z);
	std::cout << "(0, 0, 1) cross (0, 0, 3) = " << w.x << ", " << w.y << ", " << w.z << std::endl;

	Vector3D a = Vector3D(3, 0, 0);
	Vector3D b = Vector3D(1, 1, 0);
	float u = Math::dot(a, b);
	std::cout << "(3, 0, 0) dot (1, 1, 0) = " << u << std::endl;
	u = acos(u / (Math::magnitude(a) * Math::magnitude(b)));
	std::cout << "Angle is: " << u << " radians" << std::endl;
	std::cout << "Angle is: " << u * 180 / 3.14159 << " degrees." << std::endl;

	float val = Math::sqrt(2);
	std::cout << "Sqrt of 2 is: " << val << std::endl;
}

void on_resize(int w, int h) {
	window_w = w;
	window_h = h;
	scene_w = window_w;
	scene_h = window_h - 150;
	glViewport(0, 0, scene_w, scene_h);
}

int main(int argc, char* argv[]){

	mathLibraryTests();
	
	glutInit(&argc, argv);
	glutInitWindowSize(window_w, window_h);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("3D-Box");
	glEnable(GL_DEPTH_TEST);
	glutKeyboardFunc(keyboard_down);
	glutKeyboardUpFunc(keyboard_up);
	glutSpecialFunc(special_keyboard_down);
	glutSpecialUpFunc(special_keyboard_up);
	glutTimerFunc(100, timer, 30);
	glutMouseWheelFunc(mouseWheel);
	glutMouseFunc(mouseEventHandler);
	glutReshapeFunc(on_resize);
	glewExperimental = TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		cout << "glewInit failed: " << glewGetErrorString(err) << endl;
		exit(1);
	}
	init();
	glutDisplayFunc(update);

	std::cout << "OpenGL vendor: " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "OpenGL renderer: " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

	if (gui_active) {
		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO(); (void)io;
		//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
		ImGui::StyleColorsLight();
		//ImGui::StyleColorsClassic();
		ImGui_ImplGLUT_Init();
		ImGui_ImplGLUT_InstallFuncs();
		ImGui_ImplOpenGL2_Init();
	}

	glutMainLoop();

	if (gui_active) {
		ImGui_ImplOpenGL2_Shutdown();
		ImGui_ImplGLUT_Shutdown();
		ImGui::DestroyContext();
	}

	return 0;
}