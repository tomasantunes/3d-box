using namespace std;
#include "Box.h"
#include <iostream>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

Box::Box(float m_Length, float m_Width, float m_Height, vector<float> color, Vector3D position) : m_Length(m_Length), m_Width(m_Width), m_Height(m_Height), color(color), position(position) { 
	float l = m_Length;
	float w = m_Width;
	float h = m_Height;
	selected = false;
	scale = 1.0;
	
	verts = { Vector3D(0, 0, 0), Vector3D(l, 0, 0), Vector3D(l, 0, h), Vector3D(0, 0, h),
		Vector3D(l, 0, 0), Vector3D(l, w, 0), Vector3D(l, w, h), Vector3D(l, 0, h),
		Vector3D(l, w, 0), Vector3D(0, w, 0), Vector3D(0, w, h), Vector3D(l, w, h),
		Vector3D(0, w, 0), Vector3D(0, 0, 0), Vector3D(0, 0, h), Vector3D(0, w, h),
		Vector3D(0, 0, 0), Vector3D(0, w, 0), Vector3D(l, w, 0), Vector3D(l, 0, 0),
		Vector3D(0, w, h), Vector3D(0, 0, h), Vector3D(l, 0, h), Vector3D(l, w, h)
	};

	for (int i = 0; i < verts.size(); i++) {
		verts[i] = Math::add(verts[i], position);
	}
}

vector<Vertex> Box::GetVertices() {
	vector<Vertex> vertices = {};
	int j = 0;
	for (int i = 0; i < verts.size(); i++) {
		Vertex vertex;
		vertex.Position = glm::vec3(verts[i].x, verts[i].y, verts[i].z);
		vertex.selected = false;
		vertices.push_back(vertex);
	}
	return vertices;
}

vector<unsigned int> Box::GetIndices() {
	vector<unsigned int> indices = {};
	for (unsigned int i = 0; i < verts.size(); i += 4) {
		indices.push_back(i);
		indices.push_back(i+1);
		indices.push_back(i+2);
		indices.push_back(i);
		indices.push_back(i+2);
		indices.push_back(i+3);
	}
	return indices;
}

vector<unsigned int> Box::GetIndicesQuads() {
	vector<unsigned int> indices = {};
	for (unsigned int i = 0; i < verts.size(); i += 4) {
		indices.push_back(i);
		indices.push_back(i + 1);
		indices.push_back(i + 2);
		indices.push_back(i + 3);
	}
	return indices;
}

bool Box::collidesWithPoint(Vector3D point) {
	if (abs(position.x - point.x) < m_Length)
	{
		if (abs(position.y - point.y) < m_Width)
		{
			if (abs(position.z - point.z) < m_Height) {
				return true;
			}
		}
	}
	return false;
}



void Box::setPosition(Vector3D vec) {
	float l = m_Length;
	float w = m_Width;
	float h = m_Height;

	verts = { Vector3D(0, 0, 0), Vector3D(l, 0, 0), Vector3D(l, 0, h), Vector3D(0, 0, h),
		Vector3D(l, 0, 0), Vector3D(l, w, 0), Vector3D(l, w, h), Vector3D(l, 0, h),
		Vector3D(l, w, 0), Vector3D(0, w, 0), Vector3D(0, w, h), Vector3D(l, w, h),
		Vector3D(0, w, 0), Vector3D(0, 0, 0), Vector3D(0, 0, h), Vector3D(0, w, h),
		Vector3D(0, 0, 0), Vector3D(0, w, 0), Vector3D(l, w, 0), Vector3D(l, 0, 0),
		Vector3D(0, w, h), Vector3D(0, 0, h), Vector3D(l, 0, h), Vector3D(l, w, h)
	};

	position = vec;

	for (int i = 0; i < verts.size(); i++) {
		verts[i] = Math::add(verts[i], position);
	}
}

void Box::setColor(vector<float> col) {
	color = col;
}

float Box::Volume() {
	return m_Length * m_Width * m_Height;
}

float Box::SurfaceArea() {
	return 2.0f * ((m_Height * m_Width) + (m_Length * m_Height) + (m_Length * m_Width));
}

Vector3D Box::Center() {
	float l = m_Length;
	float w = m_Width;
	float h = m_Height;
	return Vector3D(verts[0].x + (l / 2), verts[0].y + (w / 2), verts[0].z + (h / 2));
}

vector<Vector3D> Box::Verts() {
	//std::cout << "verts[0] = " << verts[0].x << ", " << verts[0].y << ", " << verts[0].z << std::endl;
	return verts;
}

void Box::updateScale(float f) {
	float prev = scale;
	float next = scale + f;
	float diff = next / prev;
	for (int i = 0; i < verts.size(); i++) {
		verts[i] = Math::multiply(verts[i], diff);
	}
	scale += f;
}

void Box::Render() {
	for (int i = 0; i < 6; i++) {
		glBegin(GL_POLYGON);
		glColor3f(color[0], color[1], color[2]);
		glTranslatef(position.x, position.y, position.z);
		for (int j = 0; j < 4; j++) {
			glVertex3f(Box::Verts()[i * 4 + j].x, Box::Verts()[i * 4 + j].y, Box::Verts()[i * 4 + j].z);
			
		}
		glEnd();
		if (selected == true) {
			glBegin(GL_LINES);
			glLineWidth(3);
			glColor3f(0.3, 0.3, 0.3);
			for (int j = 0; j < 4; j++) {
				glVertex3f(Box::Verts()[i * 4 + j].x, Box::Verts()[i * 4 + j].y, Box::Verts()[i * 4 + j].z + 0.01);

			}
			glEnd();
		}

	}
}


Box::~Box()
{

}
