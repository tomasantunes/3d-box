using namespace std;
#include "Cylinder.h"
#include <iostream>
#include <cmath>
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

Cylinder::Cylinder(float base, float top, float height, float slices, float stacks, Vector3D position, vector<float> color) : base(base), top(top), height(height), slices(slices), stacks(stacks), position(position), color(color) {
	float sliceArc = 360.0f / slices;
	discPointsBottom = {};
	discPointsTop = {};
	verts = {};

	float angle = 0;
	for (int i = 0; i < slices; i++) {
		float x = base * (float)cos(glm::radians(angle));
		float z = base * (float)sin(glm::radians(angle));
		discPointsBottom.push_back(Vector3D(x, 0, z));

		x = top * (float)cos(glm::radians(angle));
		z = top * (float)sin(glm::radians(angle));

		discPointsTop.push_back(Vector3D(x, height, z));
		angle += sliceArc;
	}

	for (int i = 0; i < slices; i++) {
		Vector3D p2 = discPointsBottom[i];
		Vector3D p1;
		if (i == slices - 1) {
			p1 = discPointsBottom[0];
		}
		else {
			p1 = discPointsBottom[(i + 1) % discPointsBottom.size()];
		}
		verts.push_back(Vector3D(0, 0, 0));
		verts.push_back(Vector3D(p2.x, 0, p2.z));
		verts.push_back(Vector3D(p1.x, 0, p1.z));

		p2 = discPointsTop[i];
		if (i == slices - 1) {
			p1 = discPointsTop[0];
		}
		else {
			p1 = discPointsTop[(i + 1) % discPointsTop.size()];
		}
		verts.push_back(Vector3D(0, height, 0));
		verts.push_back(Vector3D(p1.x, height, p1.z));
		verts.push_back(Vector3D(p2.x, height, p2.z));
	}

	for (int i = 0; i < slices; i++) {
		Vector3D p1 = discPointsBottom[i];
		Vector3D p2;
		Vector3D p3 = discPointsTop[i];
		Vector3D p4;
		if (i == slices - 1) {
			p2 = discPointsBottom[0];
			p4 = discPointsTop[0];
		}
		else {
			p2 = discPointsBottom[(i + 1) % discPointsBottom.size()];
			p4 = discPointsTop[(i + 1) % discPointsTop.size()];
		}

		verts.push_back(p1);
		verts.push_back(p3);
		verts.push_back(p4);

		verts.push_back(p1);
		verts.push_back(p4);
		verts.push_back(p2);
	}
	
}

void Cylinder::Render() {
	glPushMatrix();
	glColor3f(color[0], color[1], color[2]);
	glTranslated(position.x, position.y, position.z);

	for (int i = 0; i < verts.size(); i++) {
		glVertex3f(verts[i].x, verts[i].y, verts[i].z);
	}

	glPopMatrix();
}

vector<Vertex> Cylinder::GetVertices() {
	vector<Vertex> vertices = {};
	for (int i = 0; i < verts.size(); i++) {
		Vertex vertex;
		vertex.Position = glm::vec3(verts[i].x, verts[i].y, verts[i].z);
		glm::vec2 texCoords;
		texCoords = glm::vec2(0.f, 0.f);
		vertex.TexCoords = texCoords;
		vertex.Normal = glm::vec3(1.0f, 0.f, 0.f);
		vertex.Color = glm::vec3(1.f, 0.f, 0.f);
		vertex.selected = false;
		vertices.push_back(vertex);
	}
	return vertices;
}

vector<unsigned int> Cylinder::GetIndices() {
	vector<unsigned int> indices = {};
	for (unsigned int i = 0; i < verts.size(); i++) {
		indices.push_back(i);
	}
	return indices;
}

void Cylinder::updateScale(float f) {
	float prev = scale;
	float next = scale + f;
	float diff = next / prev;
	base *= diff;
	top *= diff;
	height *= diff;
	scale += f;
}

void Cylinder::setColor(vector<float> col) {
	color = col;
}

void Cylinder::setPosition(Vector3D vec) {
	position = vec;

	for (int i = 0; i < verts.size(); i++) {
		verts[i] = Math::add(verts[i], position);
	}
}

Cylinder::~Cylinder() {

}
