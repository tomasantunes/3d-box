using namespace std;
#include "Plane.h"
#include <iostream>
#include <math.h>
#include <cmath>
#include "Math.h"
#include <vector>
#include "Vector3D.h"


Plane::Plane(float w, float h, Vector3D position, vector<float> color) : w(w), h(h), position(position), color(color) {
	scale = 1.0;
	verts.push_back(Vector3D(position.x, position.y, position.z));
	verts.push_back(Vector3D(position.x, position.y + h, position.z));
	verts.push_back(Vector3D(position.x + w, position.y + h, position.z));
	verts.push_back(Vector3D(position.x + w, position.y, position.z));
	
}

void Plane::Render() {
	glColor3f(color[0], color[1], color[2]);
	glBegin(GL_POLYGON);
	glVertex3f(position.x, position.y, position.z);
	glVertex3f(position.x + w, position.y, position.z);
	glVertex3f(position.x + w, position.y + h, position.z);
	glVertex3f(position.x, position.y + h, position.z);
	glEnd();
}

vector<Vertex> Plane::GetVertices() {
	vector<Vertex> vertices = {};
	for (int i = 0; i < verts.size(); i++) {
		Vertex vertex;
		vertex.Position = glm::vec3(verts[i].x, verts[i].y, verts[i].z);
		vertex.selected = false;
		vertices.push_back(vertex);
	}
	return vertices;
}

vector<unsigned int> Plane::GetIndices() {
	vector<unsigned int> indices = {};
	for (unsigned int i = 0; i < verts.size(); i += 4) {
		indices.push_back(i);
		indices.push_back(i+2);
		indices.push_back(i+1);
		indices.push_back(i);
		indices.push_back(i+3);
		indices.push_back(i+2);
	}
	return indices;
}

vector<unsigned int> Plane::GetIndicesQuads() {
	vector<unsigned int> indices = {};
	for (unsigned int i = 0; i < verts.size(); i += 4) {
		indices.push_back(i);
		indices.push_back(i + 3);
		indices.push_back(i + 2);
		indices.push_back(i + 1);
	}
	return indices;
}

void Plane::updateScale(float f) {
	float prev = scale;
	float next = scale + f;
	float diff = next / prev;
	scale += f;
}

void Plane::setColor(vector<float> col) {
	color = col;
}

void Plane::setPosition(Vector3D vec) {
	position = vec;
	verts = {};
	verts.push_back(Vector3D(position.x, position.y, position.z));
	verts.push_back(Vector3D(position.x, position.y + h, position.z));
	verts.push_back(Vector3D(position.x + w, position.y + h, position.z));
	verts.push_back(Vector3D(position.x + w, position.y, position.z));
}

Plane::~Plane() {

}

