using namespace std;
#pragma once
#include <GL/glew.h>
#include <gl/freeglut.h>
#include <iostream>
#include <string>
#include <vector>
#include "structs.cpp"
#include "shader.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class Texture
{
public:
	Texture(Shader s);
	~Texture();

	bool Load(const char* path);
	bool loadTextureFromPixels32(GLuint* pixels, GLuint width, GLuint height);
	GLuint LoadTexture(const char* path);
	GLuint GenerateSolidTexture(vector<float> col);
	void createBuffer();

	void freeTexture();

	void render();
	void renderQuad(GLfloat x, GLfloat y, int scene_w, int scene_h);
	void SetParameters();

	unsigned int id;
	string type;
	string path;
	GLenum format;
	Shader shader;

	GLuint getTextureID();

	GLuint textureWidth();

	GLuint textureHeight();

	GLuint* GenerateTexture1();

	vector<Vertex> vertices;
	vector<unsigned int> indices;

private:
	GLuint mTextureID;
	int mTextureWidth;
	int mTextureHeight;
	unsigned int VAO, VBO, EBO;

};


