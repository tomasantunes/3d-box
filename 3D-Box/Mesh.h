using namespace std;
#pragma once
#include <GL/glew.h>
#include <gl/freeglut.h>
#include <glm.hpp>
#include "Mesh.h"
#include "Shader.h"
#include <vector>
#include <string>
#include "Vector3D.h"
#include "Texture.h"
#include "structs.cpp"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class Mesh {
	public:
		Mesh();
		~Mesh();
		/*  Mesh Data  */
		Shader shader;
		Shader shaderSingleColor;
		vector<Vertex> vertices;
		vector<unsigned int> indices;
		vector<unsigned int> indicesQuads;
		vector<Texture> textures;
		vector<float> color;
		Vector3D position;
		GLuint default_texture;
		GLuint select_texture;
		char* texture_path;
		int vertsPerFace;
		bool selected;
		float scale_x;
		float scale_y;
		float scale_z;
		glm::vec2 TexCoords;
		bool hasModelCoords;
		string texture_type;
		GLenum tex_param1;
		vector<Vertex> selected_vertices;
		
		/*  Functions  */
		void SetPosition(glm::vec3 pos);
		void SetColor(vector<float> col);
		void Load(vector<Vertex> vertices, vector<unsigned int> indices, vector<unsigned int> indicesQuads, vector<Texture> textures, Shader shader, int vertsPerFace = 3, bool hasModelCoords = false);
		void Draw();
		void DrawVertices(glm::mat4 mv, glm::mat4 p, int scene_w, int scene_h, Shader s);
		void rotateVerticesX(float rotation);
		void rotateVerticesY(float rotation);
		void rotateVerticesZ(float rotation);
		void SetScaleX(float f);
		void SetScaleY(float f);
		void SetScaleZ(float f);
		void SetScale();
		void SetDefaultTexture(char* path);
		void SetTexCoords(glm::vec2 coords);
		void SetParameters();
		void SetTexParam(int i, GLenum value);
		void calculateNormals();
		vector<vector<Vertex>> GetEdges();
		void Subdivide();
		void setupMesh();
		
	private:
		/*  Render data  */
		unsigned int VAO, VBO, EBO, EBO2;
		/*  Functions    */
		
};

