#include "Texture.h"
#include "stb_image.h"

Texture::Texture(Shader s)
{
	mTextureID = 0;
	mTextureWidth = 0;
	mTextureHeight = 0;
	shader = s;
}

Texture::~Texture()
{
	freeTexture();
}

GLuint Texture::GenerateSolidTexture(vector<float> col) {
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	float pixels[] = { col[0], col[1], col[2] };
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2, 2, 0, GL_RGB, GL_FLOAT, pixels);
	return tex;
}

GLuint Texture::LoadTexture(const char* path) {

	GLuint textureID;
	GLuint format;
	int width, height;
	int nrComponents;
	string filename = string(path);
	

	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Read the file, call glTexImage2D with the right parameters
	stbi_set_flip_vertically_on_load(true);
	unsigned char* data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
	if (data)
	{
		format = GL_RGB;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		mTextureWidth = width;
		mTextureHeight = height;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glGetError();
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		GLenum error = glGetError();
		if (error != GL_NO_ERROR)
		{
			std::cout << "Texture failed to load: " << std::to_string(error) << std::endl;
		}
	}
	else {
		GLenum error = glGetError();
		if (error != GL_NO_ERROR)
		{
			std::cout << "Texture failed to load: " << std::to_string(error) << std::endl;
		}
	}

	return textureID;
}

GLuint* Texture::GenerateTexture1() {
	const int CHECKERBOARD_WIDTH = 128;
	const int CHECKERBOARD_HEIGHT = 128;
	const int CHECKERBOARD_PIXEL_COUNT = CHECKERBOARD_WIDTH * CHECKERBOARD_HEIGHT;
	GLuint checkerBoard[CHECKERBOARD_PIXEL_COUNT];

	for (int i = 0; i < CHECKERBOARD_PIXEL_COUNT; ++i) {
		GLubyte* colors = (GLubyte*)& checkerBoard[i];
	
		if (i / 128 & 16 ^ i % 128 & 16) {
			colors[0] = 0xFF;
			colors[1] = 0xFF;
			colors[2] = 0xFF;
			colors[3] = 0xFF;
		}
		else {
			colors[0] = 0xFF;
			colors[1] = 0x00;
			colors[2] = 0x00;
			colors[3] = 0xFF;
		}
	}

	return checkerBoard;
}

bool Texture::loadTextureFromPixels32(GLuint* pixels, GLuint width, GLuint height)
{
	freeTexture();

	mTextureWidth = width;
	mTextureHeight = height;

	glGenTextures(1, &mTextureID);

	glBindTexture(GL_TEXTURE_2D, mTextureID);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGetError();
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	GLenum error = glGetError();
	if (error != GL_NO_ERROR) {
		std::cout << "Texture failed to load" << std::endl;
		return false;
	}
	
	SetParameters();
	std::cout << "Texture id: " << mTextureID << std::endl;
	id = mTextureID;
	return true;
}

void Texture::SetParameters() {
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
}



bool Texture::Load(const char* path)
{
	id = LoadTexture(path);
	createBuffer();
	
	return true;
}

void Texture::createBuffer() {
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	vertices = {};
	indices = {};

	Vertex v1;
	v1.Position = glm::vec3(-mTextureWidth / 2, -mTextureHeight / 2, 0.0f);
	v1.TexCoords = glm::vec2(0.f, 0.f);
	v1.Normal = glm::vec3(0.f, 0.f, 1.f);
	vertices.push_back(v1);
	Vertex v2;
	v2.Position = glm::vec3(mTextureWidth / 2, -mTextureHeight / 2, 0.0f);
	v2.TexCoords = glm::vec2(1.f, 0.f);
	v2.Normal = glm::vec3(0.f, 0.f, 1.f);
	vertices.push_back(v2);
	Vertex v3;
	v3.Position = glm::vec3(mTextureWidth / 2, mTextureHeight / 2, 0.0f);
	v3.TexCoords = glm::vec2(1.0f, 1.0f);
	v3.Normal = glm::vec3(0.f, 0.f, 1.f);
	vertices.push_back(v3);
	Vertex v4;
	v4.Position = glm::vec3(-mTextureWidth / 2, mTextureHeight / 2, 0.0f);
	v4.TexCoords = glm::vec2(0.f, 1.f);
	v4.Normal = glm::vec3(0.f, 0.f, 1.f);
	vertices.push_back(v4);

	for (unsigned int i = 0; i < vertices.size(); i++) {
		indices.push_back(i);
	}
	GLenum error = glGetError();
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
	if (error != GL_NO_ERROR)
	{
		std::cout << "Error: " << std::to_string(error) << std::endl;
	}

	error = glGetError();
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	if (error != GL_NO_ERROR)
	{
		std::cout << "Error: " << std::to_string(error) << std::endl;
	}

	error = glGetError();
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
	if (error != GL_NO_ERROR)
	{
		std::cout << "Error: " << std::to_string(error) << std::endl;
	}

	error = glGetError();
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));
	if (error != GL_NO_ERROR)
	{
		std::cout << "Error: " << std::to_string(error) << std::endl;
	}

	error = glGetError();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
	if (error != GL_NO_ERROR)
	{
		std::cout << "Error: " << std::to_string(error) << std::endl;
	}

	error = glGetError();
	if (error != GL_NO_ERROR)
	{
		std::cout << "Error: " << std::to_string(error) << std::endl;
	}
}

void Texture::freeTexture()
{
	if (mTextureID != 0) {
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
	}
	glBindTexture(GL_TEXTURE_2D, NULL);
	mTextureWidth = 0;
	mTextureHeight = 0;
}

void Texture::render()
{
	if (mTextureID != 0)
	{
		glBindTexture(GL_TEXTURE_2D, mTextureID);
	}
}

void drawRectangle2(int x, int y, int w, int h) {
	glBegin(GL_QUADS);
	glVertex2f(x, y);
	glVertex2f(x + w, y);
	glVertex2f(x + w, y + h);
	glVertex2f(x, y + h);
	glEnd();
}

void Texture::renderQuad(GLfloat x, GLfloat y, int scene_w, int scene_h) {
	if (id != 0)
	{
		createBuffer();
		//glTranslatef(x, y, 0.f);
		glGetError();
		//createBuffer();
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, id);
		SetParameters();
		glGetError();
		
		shader.use();
		shader.setInt("texture_diffuse1", 0);
		glm::mat4 ortho = glm::ortho(-(float)scene_w / 2, (float)scene_w / 2, -(float)scene_h / 2, (float)scene_h / 2, -25.f, 25.f);
		shader.setMat4("projection", ortho);
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		//drawRectangle2(0, 0, 50, 50);
		glDrawElements(GL_QUADS, indices.size(), GL_UNSIGNED_INT, 0);
		GLenum error = glGetError();
		while (error != GL_NO_ERROR) {
			std::cout << "Error: " + std::to_string(error) << std::endl;
			error = glGetError();
		}
		glBindTexture(GL_TEXTURE_2D, 0);
		//glActiveTexture(GL_TEXTURE0);
		glBindVertexArray(0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDeleteVertexArrays(1, &VAO);
		glDeleteBuffers(1, &VBO);
		glDeleteBuffers(1, &EBO);
		shader.active(false);
	}
}

GLuint Texture::getTextureID()
{
	return mTextureID;
}

GLuint Texture::textureWidth()
{
	return mTextureWidth;
}

GLuint Texture::textureHeight()
{
	return mTextureHeight;
}
