using namespace std;
#include "Vector3D.h"
#include "Matrix.h"
#include <math.h>
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include <iostream> 
#include <algorithm>

#define PI = 3.1415;

namespace Math {

	Vector3D add(Vector3D a, Vector3D b){

		Vector3D sum = Vector3D();
		sum.x = a.x + b.x;
		sum.y = a.y + b.y;
		sum.z = a.z + b.z;

		return sum;
	}

	glm::vec3 add2(glm::vec3 a, glm::vec3 b) {

		glm::vec3 sum;
		sum.x = a.x + b.x;
		sum.y = a.y + b.y;
		sum.z = a.z + b.z;

		return sum;
	}

	glm::vec3 normalize(glm::vec3 vec)
	{
		const float Length = sqrtf(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);

		vec.x /= Length;
		vec.y /= Length;
		vec.z /= Length;

		return vec;
	}

	glm::vec4 rotateAround(glm::vec4 aPointToRotate, glm::vec4 aRotationCenter, glm::mat4x4 aRotationMatrix, glm::mat4 model) {
		glm::mat4x4 translate = glm::translate(model, glm::vec3(aRotationCenter.x, aRotationCenter.y, aRotationCenter.z));
		glm::mat4x4 invTranslate = glm::inverse(translate);

		// 1) Translate the object to the center
		// 2) Make the rotation
		// 3) Translate the object back to its original location

		glm::mat4x4 transform = translate * aRotationMatrix * invTranslate;

		return transform * aPointToRotate;
	}

	Vector3D subtract(Vector3D b, Vector3D a) {

		Vector3D diff = Vector3D();
		diff.x = b.x - a.x;
		diff.y = b.y - a.y;
		diff.z = b.z - a.z;

		return diff;
	}

	glm::vec3 subtract2(glm::vec3 b, glm::vec3 a) {

		glm::vec3 diff;
		diff.x = b.x - a.x;
		diff.y = b.y - a.y;
		diff.z = b.z - a.z;

		return diff;
	}

	Vector3D multiply(Vector3D a, float c) {

		Vector3D mul = Vector3D();
		mul.x = a.x * c;
		mul.y = a.y * c;
		mul.z = a.z * c;

		return mul;
	}

	glm::vec3 multiply2(glm::vec3 a, float c) {

		glm::vec3 mul = glm::vec3();
		mul.x = a.x * c;
		mul.y = a.y * c;
		mul.z = a.z * c;

		return mul;
	}

	Vector3D rotateX(Vector3D a, float theta) {
		Vector3D b = Vector3D();
		float sin_t = sin(theta);
		float cos_t = cos(theta);

		float y = a.y;
		float z = a.z;

		b.x = a.x;
		b.y = y * cos_t - z * sin_t;
		b.z = z * cos_t + y * sin_t;

		return b;
	};

	Vector3D rotateY(Vector3D a, float theta) {
		Vector3D b = Vector3D();
		float sin_t = sin(theta);
		float cos_t = cos(theta);

		float x = a.x;
		float z = a.z;

		b.y = a.y;
		b.x = x * cos_t - z * sin_t;
		b.z = z * cos_t + x * sin_t;

		return b;
	};

	Vector3D rotateZ(Vector3D a, float theta) {
		Vector3D b = Vector3D();
		float sin_t = sin(theta);
		float cos_t = cos(theta);

		float y = a.y;
		float x = a.x;

		b.z = a.z;
		b.x = x * cos_t - y * sin_t;
		b.y = y * cos_t + x * sin_t;

		return b;
	};

	Vector3D rotate2(Vector3D a, Vector3D center, float theta, float phi) {
		Vector3D b = Vector3D();
		// Rotation matrix coefficients
		float ct = cos(theta);
		float st = sin(theta);
		float cp = cos(phi);
		float sp = sin(phi);

		// Rotation
		float x = a.x - center.x;
		float y = a.y - center.y;
		float z = a.z - center.z;

		b.x = ct * x - st * cp * y + st * sp * z + center.x;
		b.y = st * x + ct * cp * y - ct * sp * z + center.y;
		b.z = sp * y + cp * z + center.z;

		return b;
	}

	float distance(Vector3D a, Vector3D b) {
		float deltaX = a.x - b.x;
		float deltaY = a.y - a.y;
		float deltaZ = a.z - a.z;
		return sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
	}

	/*
	Project A onto B.
	A dot B = |A| |B| cos(theta)
	Can find angle between A and B
	*/
	float dot(Vector3D a, Vector3D b) {

		return a.x * b.x + a.y * b.y + a.z * b.z;
	}

	float dot2(glm::vec3 a, glm::vec3 b) {
		return a.x * b.x + a.y * b.y + a.z * b.z;
	}

	//Find the determinant.
	float det(float a, float b, float c, float d) {

		return a*d - b*c;
	}

	/*
		i   j   k
		ax  ay  az
		bx  by  bz

			| ay  az |            | ax  az |            | ax  ay |
	   det  | by  bz | * i -  det | bx  bz |  * j + det | bx  by | * k

			| a  b |
		det	| c  d | = ad - bc

		Returns a vector that is perpendicular to A and B.
		Used to find normal vectors on mesh faces in graphics, for example.

		| A cross B | = |A| |B| sin(theta)
	*/
	Vector3D cross(Vector3D a, Vector3D b) {

		Vector3D crossProduct = Vector3D();
		crossProduct.x = det(a.y, a.z, b.y, b.z);
		crossProduct.y = -1 * det(a.x, a.z, b.x, b.z);
		crossProduct.z = det(a.x, a.y, b.x, b.y);

		return crossProduct;
	}

	glm::vec3 cross2(glm::vec3 a, glm::vec3 b) {

		glm::vec3 crossProduct;
		crossProduct.x = det(a.y, a.z, b.y, b.z);
		crossProduct.y = -1 * det(a.x, a.z, b.x, b.z);
		crossProduct.z = det(a.x, a.y, b.x, b.y);

		return crossProduct;
	}

	float normalize(float x)
	{
		if (x > 1) {
			return 1.f;
		}
		else if (x < -1.f) {
			return -1.f;
		}
		else {
			return x;
		}
	}

	glm::vec3 cross3(glm::vec3 a, glm::vec3 b) {

		glm::vec3 crossProduct;
		crossProduct.x = normalize(det(a.y, a.z, b.y, b.z));
		crossProduct.y = normalize(-1 * det(a.x, a.z, b.x, b.z));
		crossProduct.z = normalize(-1 * det(a.x, a.y, b.x, b.y));

		return crossProduct;
	}

	//Absolute value of A.
	float abs(float a) {
		if (a < 0) {
			return (-1 * a);
		}
		else {
			return a;
		}
	}

	//Newton's Method: Xn+1 = Xn - f(Xn) / f'(Xn), where f(x) = x^2 - val and f'(x) = 2x. 
	//TO-DO: Alternate overloaded function with margin of error as parameter
	float sqrtNewton(float val, float estimate) {
		
		float error = abs(estimate*estimate - val);

		if (error <= 0.0001) {
			return estimate;
		}
		else {

			float newEstimate = (((val / estimate) + estimate) / 2);
			return sqrtNewton(val, newEstimate);
		}
	}

	//Finds square root using Newton's Method. 
	float sqrt(float x){

		if (x < 0) {
			return -1.0;
		}
		else {
			return sqrtNewton(x, 1.0);
		}
	}

	//Finds |v| = sqrt(v.x^2 + v.y^2 + v.z^2).
	float magnitude(Vector3D v) {
		return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
	}

	

}