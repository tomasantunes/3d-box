using namespace std;
#pragma once
#include "Mesh.h"
#include <vector>
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include "Math.h"
#include "stb_image.h"

Mesh::Mesh() {
	
}

void print(string str) {
	std::cout << str << std::endl;
}

void printf(float f) {
	std::cout << f << std::endl;
}

void Mesh::Load(vector<Vertex> vertices, vector<unsigned int> indices, vector<unsigned int> indicesQuads, vector<Texture> textures, Shader shader, int vertsPerFace, bool hasModelCoords) {
	this->vertices = vertices;
	this->indices = indices;
	this->indicesQuads = indicesQuads;
	this->position = Vector3D(0.0f, 0.0f, 0.0f);
	this->textures = textures;
	this->shader = shader;
	this->shaderSingleColor = shaderSingleColor;
	this->vertsPerFace = vertsPerFace;
	this->selected = false;
	this->scale_x = 1.f;
	this->scale_y = 1.f;
	this->scale_z = 1.f;
	this->select_texture = 0;
	this->default_texture = 0;
	this->hasModelCoords = hasModelCoords;
	this->TexCoords = glm::vec2(0.f, 0.f);
	this->tex_param1 = GL_REPEAT;
	this->texture_path = "C:\\Users\\tomas\\Documents\\HappieEngine\\grey.jpg";
	this->texture_type = "image";
	setupMesh();
}

void Mesh::SetTexParam(int i, GLenum value) {
	if (i == 1) {
		tex_param1 = value;
	}
}

void Mesh::SetPosition(glm::vec3 pos) {
	this->position = Vector3D(pos.x, pos.y, pos.z);
	for (int i = 0; i < this->vertices.size(); i++) {
		this->vertices[i].Position = Math::add2(this->vertices[i].Position, pos);
	}
	setupMesh();
}

void Mesh::SetTexCoords(glm::vec2 coords) {
	TexCoords = coords;
	if (!hasModelCoords) {
		for (unsigned int i = 0; i < this->vertices.size(); i++) {
			this->vertices[i].TexCoords = TexCoords;
		}
		setupMesh();
	}
}

void Mesh::SetScaleX(float f) {
	scale_x = f;
}

void Mesh::SetScaleY(float f) {
	scale_y = f;
}

void Mesh::SetScaleZ(float f) {
	scale_z = f;
}

void Mesh::SetScale() {
	for (int i = 0; i < vertices.size(); i++) {
		vertices[i].Position.x = vertices[i].Position.x * scale_x;
		vertices[i].Position.y = vertices[i].Position.y * scale_y;
		vertices[i].Position.z = vertices[i].Position.z * scale_z;
	}
	scale_x = 1.f;
	scale_y = 1.f;
	scale_z = 1.f;
	setupMesh();
}

GLuint GenerateSolidTexture(vector<float> col) {
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	float pixels[] = { col[0], col[1], col[2] };
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2, 2, 0, GL_RGB, GL_FLOAT, pixels);
	return tex;
}

GLuint LoadTexture(const char* path) {

	GLuint textureID;
	GLuint format;
	int width, height;
	int nrComponents;
	string filename = string(path);

	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Read the file, call glTexImage2D with the right parameters
	unsigned char* data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
	if (data)
	{
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glGetError();
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		GLenum error = glGetError();
		if (error != GL_NO_ERROR)
		{
			std::cout << "Texture failed to load: " << std::to_string(error) << std::endl;
		}
	}

	return textureID;
}

void Mesh::SetDefaultTexture(char* path) {
	texture_path = path;
	default_texture = LoadTexture(texture_path);
}

void Mesh::setupMesh()
{
	//std::cout << "Vertices size: " << vertices.size() << std::endl;
	if (vertices.size() > 0) {
		calculateNormals();
		glDeleteVertexArrays(1, &VAO);
		glDeleteBuffers(1, &VBO);
		glDeleteBuffers(1, &EBO);
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);

		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);

		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
	}
	//std::cout << "Indices size: " << indices.size() << std::endl;
	if (indices.size() > 0) {
		// vertex positions
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, vertsPerFace, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
		// vertex normals
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, vertsPerFace, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
		// vertex texture coords
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, vertsPerFace, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
	}
	if (textures.size() < 1 && default_texture == 0) {
		texture_path = "C:\\Users\\tomas\\Documents\\HappieEngine\\grey.jpg";
		default_texture = LoadTexture(texture_path);
	}
	if (select_texture == 0) {
		select_texture = GenerateSolidTexture({ 0.0f, 0.0f, 0.0f });
	}
}

Material loadDefaultMaterial() {
	Material material;
	aiColor3D color(0.f, 0.f, 0.f);
	float shininess = 0.1f;

	material.Diffuse = glm::vec3(color.r, color.b, color.g);

	material.Ambient = glm::vec3(color.r, color.b, color.g);

	material.Specular = glm::vec3(color.r, color.b, color.g);

	return material;
}

void Mesh::SetParameters() {
	if (tex_param1 == GL_REPEAT) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
	else {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}

void Mesh::SetColor(vector<float> col) {
	color = col;
	default_texture = GenerateSolidTexture(col);
}

void Mesh::rotateVerticesX(float rotation) {
	if (rotation > 0) {
		for (int i = 0; i < vertices.size(); i++) {
			glm::mat4 model = glm::mat4(1.0);
			glm::vec4 v4In(vertices[i].Position.x, vertices[i].Position.y, vertices[i].Position.z, 1.0f);
			glm::vec4 v4RotCenter(position.x, position.y, position.z, 1.0f);
			glm::vec3 v3RotAxis(1.0f, 0.0f, 0.0f);
			float angleRad = glm::radians(rotation);
			glm::mat4x4 matRot = glm::rotate(model, angleRad, v3RotAxis);
			glm::vec4 b = Math::rotateAround(v4In, v4RotCenter, matRot, model);
			glm::vec3 c = glm::vec3(b.x, b.y, b.z);
			vertices[i].Position = c;
		}
		setupMesh();
	}
}

void Mesh::rotateVerticesY(float rotation) {
	if (rotation > 0) {
		for (int i = 0; i < vertices.size(); i++) {
			glm::mat4 model = glm::mat4(1.0);
			glm::vec4 v4In(vertices[i].Position.x, vertices[i].Position.y, vertices[i].Position.z, 1.0f);
			glm::vec4 v4RotCenter(position.x, position.y, position.z, 1.0f);
			glm::vec3 v3RotAxis(0.0f, 1.0f, 0.0f);
			float angleRad = glm::radians(rotation);
			glm::mat4x4 matRot = glm::rotate(model, angleRad, v3RotAxis);
			glm::vec4 b = Math::rotateAround(v4In, v4RotCenter, matRot, model);
			glm::vec3 c = glm::vec3(b.x, b.y, b.z);
			vertices[i].Position = c;
		}
		setupMesh();
	}
}

void Mesh::rotateVerticesZ(float rotation) {
	if (rotation > 0) {
		for (int i = 0; i < vertices.size(); i++) {
			glm::mat4 model = glm::mat4(1.0);
			glm::vec4 v4In(vertices[i].Position.x, vertices[i].Position.y, vertices[i].Position.z, 1.0f);
			glm::vec4 v4RotCenter(position.x, position.y, position.z, 1.0f);
			glm::vec3 v3RotAxis(0.0f, 0.0f, 1.0f);
			float angleRad = glm::radians(rotation);
			glm::mat4x4 matRot = glm::rotate(model, angleRad, v3RotAxis);
			glm::vec4 b = Math::rotateAround(v4In, v4RotCenter, matRot, model);
			glm::vec3 c = glm::vec3(b.x, b.y, b.z);
			vertices[i].Position = c;
		}
		setupMesh();
	}
}

void Mesh::calculateNormals() {
	if (vertsPerFace == 3) {
		for (int i = 0; i < this->indices.size(); i += 3) {
			Vertex v1 = this->vertices[indices[i]];
			Vertex v2 = this->vertices[indices[i + 1]];
			Vertex v3 = this->vertices[indices[i + 2]];
			this->vertices[indices[i]].Normal = Math::cross3(Math::subtract2(v2.Position, v1.Position), Math::subtract2(v3.Position, v1.Position));
			this->vertices[indices[i + 1]].Normal = Math::cross3(Math::subtract2(v3.Position, v2.Position), Math::subtract2(v1.Position, v2.Position));
			this->vertices[indices[i + 2]].Normal = Math::cross3(Math::subtract2(v1.Position, v3.Position), Math::subtract2(v2.Position, v3.Position));
		}
	}
	else if (vertsPerFace == 4) {
		for (int i = 0; i < this->vertices.size(); i += 4) {
			Vertex v1 = this->vertices[i];
			Vertex v2 = this->vertices[i + 1];
			Vertex v3 = this->vertices[i + 2];
			Vertex v4 = this->vertices[i + 3];
			this->vertices[i].Normal = Math::cross3(Math::subtract2(v2.Position, v1.Position), Math::subtract2(v3.Position, v1.Position));
			this->vertices[i + 1].Normal = Math::cross3(Math::subtract2(v3.Position, v2.Position), Math::subtract2(v1.Position, v2.Position));
			this->vertices[i + 2].Normal = Math::cross3(Math::subtract2(v4.Position, v3.Position), Math::subtract2(v2.Position, v3.Position));
			this->vertices[i + 3].Normal = Math::cross3(Math::subtract2(v1.Position, v4.Position), Math::subtract2(v3.Position, v4.Position));
		}
	}
}

vector<vector<Vertex>> Mesh::GetEdges() {
	vector<vector<Vertex>> edges = {};

	for (int i = 0; i < indices.size() - 1; i+=3) {
		Vertex v1 = vertices[indices[i]];
		Vertex v2 = vertices[indices[i+1]];
		vector<Vertex> edge1 = { v1, v2 };
		edges.push_back(edge1);
		v1 = vertices[indices[i+1]];
		v2 = vertices[indices[i+2]];
		vector<Vertex> edge2 = { v1, v2 };
		edges.push_back(edge2);
		v1 = vertices[indices[i+2]];
		v2 = vertices[indices[i]];
		vector<Vertex> edge3 = { v1, v2 };
		edges.push_back(edge3);
	}

	return edges;
}

void Mesh::Subdivide() {
	vector<Vertex> new_verts = {};
	vector<unsigned int> new_indices = {};
	vector<unsigned int> new_indicesQuads = {};
	if (vertsPerFace == 3) {
		int triangle_count = 0;

		for (int i = 0; i < indices.size(); i += 3) {
			Vertex v1 = vertices[indices[i]];
			Vertex v2 = vertices[indices[i + 1]];
			Vertex v3 = vertices[indices[i + 2]];

			Vertex mid1;
			mid1.Position = glm::vec3((v1.Position.x + v2.Position.x) / 2, (v1.Position.y + v2.Position.y) / 2, (v1.Position.z + v2.Position.z) / 2);
			mid1.selected = false;
			Vertex mid2;
			mid2.Position = glm::vec3((v2.Position.x + v3.Position.x) / 2, (v2.Position.y + v3.Position.y) / 2, (v2.Position.z + v3.Position.z) / 2);
			mid2.selected = false;
			Vertex mid3;
			mid3.Position = glm::vec3((v3.Position.x + v1.Position.x) / 2, (v3.Position.y + v1.Position.y) / 2, (v3.Position.z + v1.Position.z) / 2);
			mid3.selected = false;

			new_verts.push_back(v1);
			new_verts.push_back(mid1);
			new_verts.push_back(v2);
			new_verts.push_back(mid2);
			new_verts.push_back(v3);
			new_verts.push_back(mid3);

			new_indices.push_back(triangle_count * 6);
			new_indices.push_back(triangle_count * 6 + 1);
			new_indices.push_back(triangle_count * 6 + 5);
			new_indices.push_back(triangle_count * 6 + 1);
			new_indices.push_back(triangle_count * 6 + 3);
			new_indices.push_back(triangle_count * 6 + 5);
			new_indices.push_back(triangle_count * 6 + 1);
			new_indices.push_back(triangle_count * 6 + 2);
			new_indices.push_back(triangle_count * 6 + 3);
			new_indices.push_back(triangle_count * 6 + 5);
			new_indices.push_back(triangle_count * 6 + 3);
			new_indices.push_back(triangle_count * 6 + 4);

			triangle_count++;
		}

		vertices = new_verts;
		indices = new_indices;
		SetTexCoords(TexCoords);
		setupMesh();
	}
	else if (vertsPerFace == 4) {
		int quad_count = 0;

		for (int i = 0; i < indicesQuads.size(); i += 4) {
			Vertex v1 = vertices[indicesQuads[i]];
			Vertex v2 = vertices[indicesQuads[i + 1]];
			Vertex v3 = vertices[indicesQuads[i + 2]];
			Vertex v4 = vertices[indicesQuads[i + 3]];

			Vertex mid1;
			mid1.Position = glm::vec3((v1.Position.x + v2.Position.x) / 2, (v1.Position.y + v2.Position.y) / 2, (v1.Position.z + v2.Position.z) / 2);
			mid1.selected = false;
			Vertex mid2;
			mid2.Position = glm::vec3((v2.Position.x + v3.Position.x) / 2, (v2.Position.y + v3.Position.y) / 2, (v2.Position.z + v3.Position.z) / 2);
			mid2.selected = false;
			Vertex mid3;
			mid3.Position = glm::vec3((v3.Position.x + v4.Position.x) / 2, (v3.Position.y + v4.Position.y) / 2, (v3.Position.z + v4.Position.z) / 2);
			mid3.selected = false;
			Vertex mid4;
			mid4.Position = glm::vec3((v4.Position.x + v1.Position.x) / 2, (v4.Position.y + v1.Position.y) / 2, (v4.Position.z + v1.Position.z) / 2);
			mid4.selected = false;
			Vertex mid5;
			mid5.Position = glm::vec3((v1.Position.x + v2.Position.x + v3.Position.x + v4.Position.x) / 4, (v1.Position.y + v2.Position.y + v3.Position.y + v4.Position.y) / 4, (v1.Position.z + v2.Position.z + v3.Position.z + v4.Position.z) / 4);
			mid5.selected = false;

			new_verts.push_back(v1);
			new_verts.push_back(mid1);
			new_verts.push_back(mid5);
			new_verts.push_back(mid4);
			new_verts.push_back(mid1);
			new_verts.push_back(v2);
			new_verts.push_back(mid2);
			new_verts.push_back(mid5);
			new_verts.push_back(mid5);
			new_verts.push_back(mid2);
			new_verts.push_back(v3);
			new_verts.push_back(mid3);
			new_verts.push_back(mid4);
			new_verts.push_back(mid5);
			new_verts.push_back(mid3);
			new_verts.push_back(v4);

			new_indices.push_back(quad_count * 16);
			new_indices.push_back(quad_count * 16 + 1);
			new_indices.push_back(quad_count * 16 + 2);
			new_indices.push_back(quad_count * 16);
			new_indices.push_back(quad_count * 16 + 2);
			new_indices.push_back(quad_count * 16 + 3);
			new_indices.push_back(quad_count * 16 + 1);
			new_indices.push_back(quad_count * 16 + 5);
			new_indices.push_back(quad_count * 16 + 6);
			new_indices.push_back(quad_count * 16 + 1);
			new_indices.push_back(quad_count * 16 + 6);
			new_indices.push_back(quad_count * 16 + 7);
			new_indices.push_back(quad_count * 16 + 8);
			new_indices.push_back(quad_count * 16 + 9);
			new_indices.push_back(quad_count * 16 + 10);
			new_indices.push_back(quad_count * 16 + 8);
			new_indices.push_back(quad_count * 16 + 10);
			new_indices.push_back(quad_count * 16 + 11);
			new_indices.push_back(quad_count * 16 + 12);
			new_indices.push_back(quad_count * 16 + 13);
			new_indices.push_back(quad_count * 16 + 14);
			new_indices.push_back(quad_count * 16 + 12);
			new_indices.push_back(quad_count * 16 + 14);
			new_indices.push_back(quad_count * 16 + 15);

			new_indicesQuads.push_back(quad_count * 16);
			new_indicesQuads.push_back(quad_count * 16 + 1);
			new_indicesQuads.push_back(quad_count * 16 + 2);
			new_indicesQuads.push_back(quad_count * 16 + 3);
			new_indicesQuads.push_back(quad_count * 16 + 4);
			new_indicesQuads.push_back(quad_count * 16 + 5);
			new_indicesQuads.push_back(quad_count * 16 + 6);
			new_indicesQuads.push_back(quad_count * 16 + 7);
			new_indicesQuads.push_back(quad_count * 16 + 8);
			new_indicesQuads.push_back(quad_count * 16 + 9);
			new_indicesQuads.push_back(quad_count * 16 + 10);
			new_indicesQuads.push_back(quad_count * 16 + 11);
			new_indicesQuads.push_back(quad_count * 16 + 12);
			new_indicesQuads.push_back(quad_count * 16 + 13);
			new_indicesQuads.push_back(quad_count * 16 + 14);
			new_indicesQuads.push_back(quad_count * 16 + 15);

			quad_count++;
		}

		vertices = new_verts;
		indices = new_indices;
		indicesQuads = new_indicesQuads;
		SetTexCoords(TexCoords);
		setupMesh();
	}
}
/*
void drawRectangle2(int x, int y, int w, int h) {
	glBegin(GL_QUADS);
	glVertex2f(x, y);
	glVertex2f(x + w, y);
	glVertex2f(x + w, y + h);
	glVertex2f(x, y + h);
	glEnd();
}
*/
void drawCircle(float x, float y, float z, float radius) {
	const int sides = 20;

	glBegin(GL_TRIANGLE_FAN);

	for (int a = 0; a < 360; a += 360 / sides)
	{
		double heading = a * 3.1415926535897932384626433832795 / 180;
		glVertex2f(cos(heading) * radius + x, sin(heading) * radius + y);
	}

	glEnd();
}

void Mesh::Draw() {
	setupMesh();
	// bind appropriate textures
	unsigned int diffuseNr = 1;
	unsigned int specularNr = 1;
	unsigned int normalNr = 1;
	unsigned int heightNr = 1;
	
	//std::cout << "Textures size: " << textures.size() << std::endl;
	if (textures.size() > 0) {

		for (unsigned int i = 0; i < textures.size(); i++)
		{
			glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
			// retrieve texture number (the N in diffuse_textureN)
			string number;
			string name = textures[i].type;
			if (name == "texture_diffuse")
				number = std::to_string(diffuseNr++);
			else if (name == "texture_specular")
				number = std::to_string(specularNr++); // transfer unsigned int to stream
			else if (name == "texture_normal")
				number = std::to_string(normalNr++); // transfer unsigned int to stream
			else if (name == "texture_height")
				number = std::to_string(heightNr++); // transfer unsigned int to stream
													 // now set the sampler to the correct texture unit
			glUniform1i(glGetUniformLocation(shader.ID, (name + number).c_str()), textures[i].id);
			glBindTexture(GL_TEXTURE_2D, textures[i].id);
		}
	}
	else {
		glBindTexture(GL_TEXTURE_2D, default_texture);
		SetParameters();
	}

	

	// std::cout << "Indices size: " << indices.size() << std::endl;
	if (indices.size() > 0) {
		// draw mesh
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		//glGetError();
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
		
		GLenum error = glGetError();
		while (error != GL_NO_ERROR) {
			std::cout << "Error: " + std::to_string(error) << std::endl;
			error = glGetError();
		}
		
		if (selected == true) {
			glBindTexture(GL_TEXTURE_2D, select_texture);
			SetParameters();
			
			/*
			for (int i = 0; i < indices.size(); i++) {
				Vertex vert = vertices[indices[i]];
				if (vert.selected == true) {
					drawCircle(vert.Position.x, vert.Position.y, vert.Position.z, 5);
				}
			}
			*/
			
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			if (vertsPerFace == 4) {
				for (int i = 0; i < indicesQuads.size(); i += 4) {
					
					glLineWidth(2.5);
					glColor3f(0.3, 0.3, 0.3);
					glm::vec3 v1 = vertices[indicesQuads[i]].Position;
					glm::vec3 v2 = vertices[indicesQuads[i + 1]].Position;
					glm::vec3 v3 = vertices[indicesQuads[i + 2]].Position;
					glm::vec3 v4 = vertices[indicesQuads[i + 3]].Position;
					glBegin(GL_LINES);
					glVertex3f(v1.x, v1.y, v1.z);
					glVertex3f(v2.x, v2.y, v2.z);
					glVertex3f(v2.x, v2.y, v2.z);
					glVertex3f(v3.x, v3.y, v3.z);
					glVertex3f(v3.x, v3.y, v3.z);
					glVertex3f(v4.x, v4.y, v4.z);
					glVertex3f(v4.x, v4.y, v4.z);
					glVertex3f(v1.x, v1.y, v1.z);
					glEnd();
				}
			}
			else if (vertsPerFace == 3) {
				glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
			}
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			
		}

		// set everything back to default
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindVertexArray(0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}

void Mesh::DrawVertices(glm::mat4 mv, glm::mat4 p, int scene_w, int scene_h, Shader s) {
	// std::cout << "Indices size: " << indices.size() << std::endl;
	
	if (indices.size() > 0) {
		for (int i = 0; i < vertices.size(); i++) {
			Vertex vert = vertices[i];
			glm::vec3 point1 = glm::vec3(vert.Position.x, vert.Position.y, vert.Position.z);
			glm::vec4 viewport(0.0f, 0.0f, (float)scene_w, (float)scene_h);
			glm::vec3 projected = glm::project(point1, mv, p, viewport);

			if (vert.selected == false) {
				glm::vec3 color1 = glm::vec3(0.0, 0.5, 1.0);
				s.setVec3("color_in", color1);
			}
			else {
				glm::vec3 color2 = glm::vec3(1.0, 0.0, 0.0);
				s.setVec3("color_in", color2);
			}

			drawCircle(projected.x, scene_h - projected.y, 0.f, 10.f);
		}
	}

}

Mesh::~Mesh()
{
}
