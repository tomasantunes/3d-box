#pragma once
#include <iostream>
#include <string>
#include <map>
#include "structs.cpp"

class ImGuiSettings
{
	public:
		ImGuiSettings();
		~ImGuiSettings();
		bool toolbar_active = true;
		bool box_active = false;
		bool cylinder_active = false;
		bool plane_active = false;
		bool sphere_active = false;
		bool model_active = false;
		bool color_active = false;
		bool vertex_mode = false;
		bool export_active = false;
		bool save_render_active = false;
		bool image_dialog_active = false;
		string projection;
		string view;
		char box_x[256];
		char box_y[256];
		char box_z[256];
		char cylinder_x[256];
		char cylinder_y[256];
		char cylinder_z[256];
		char base[256];
		char top[256];
		char height[256];
		char plane_x[256];
		char plane_y[256];
		char plane_z[256];
		char sphere_x[256];
		char sphere_y[256];
		char sphere_z[256];
		char sphere_r[256];
		char sphere_slices[256];
		char sphere_stacks[256];
		int selectedMeshIndex;
		Properties properties;

	private:

};

