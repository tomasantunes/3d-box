#ifndef STRUCTS
#define STRUCTS
using namespace std;
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include <string>

struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
	glm::vec3 Bitangent;
	glm::vec3 Tangent;
	glm::vec3 Color;
	bool selected;
	bool backface = false;
};

struct Material {
	glm::vec3 Diffuse;
	glm::vec3 Specular;
	glm::vec3 Ambient;
	float Shininess;
};

struct Properties {
	char x[256];
	char y[256];
	char z[256];
	char rotate_x[256];
	char rotate_y[256];
	char rotate_z[256];
	float color[4];
	char scale_x[256];
	char scale_y[256];
	char scale_z[256];
	char texture_path[999];
	char texcoords_x[256];
	char texcoords_y[256];
};

#endif