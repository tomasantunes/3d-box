#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

namespace Math{

	Vector3D add(Vector3D a, Vector3D b);
	glm::vec3 add2(glm::vec3 a, glm::vec3 b);
	Vector3D subtract(Vector3D b, Vector3D a);
	glm::vec3 subtract2(glm::vec3 b, glm::vec3 a);
	Vector3D multiply(Vector3D a, float c);
	glm::vec3 multiply2(glm::vec3 a, float c);
	Vector3D rotateX(Vector3D a, float theta);
	Vector3D rotateY(Vector3D a, float theta);
	Vector3D rotateZ(Vector3D a, float theta);
	Vector3D rotate2(Vector3D a, Vector3D center, float theta, float phi);
	glm::vec4 rotateAround(glm::vec4 aPointToRotate, glm::vec4 aRotationCenter, glm::mat4x4 aRotationMatrix, glm::mat4 model);
	glm::vec3 normalize(glm::vec3 vec);

	float dot(Vector3D a, Vector3D b);
	float det(float a, float b, float c, float d);
	Vector3D cross(Vector3D a, Vector3D b);
	glm::vec3 cross2(glm::vec3 a, glm::vec3 b);
	glm::vec3 cross3(glm::vec3 a, glm::vec3 b);
	float dot2(glm::vec3 a, glm::vec3 b);

	float sqrtNewton(float val, float estimate);
	float sqrt(float x);

	float abs(float a);
	float magnitude(Vector3D v);
};