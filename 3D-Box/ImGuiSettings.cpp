#include "ImGuiSettings.h"

ImGuiSettings::ImGuiSettings() {
	memset(box_x, 0, sizeof(box_x));
	memset(box_y, 0, sizeof(box_y));
	memset(box_z, 0, sizeof(box_z));
	memset(cylinder_x, 0, sizeof(cylinder_x));
	memset(cylinder_y, 0, sizeof(cylinder_y));
	memset(cylinder_z, 0, sizeof(cylinder_z));
	memset(base, 0, sizeof(base));
	memset(top, 0, sizeof(top));
	memset(height, 0, sizeof(height));
	memset(plane_x, 0, sizeof(plane_x));
	memset(plane_y, 0, sizeof(plane_y));
	memset(plane_z, 0, sizeof(plane_z));
	memset(sphere_x, 0, sizeof(sphere_x));
	memset(sphere_y, 0, sizeof(sphere_y));
	memset(sphere_z, 0, sizeof(sphere_z));
	memset(sphere_r, 0, sizeof(sphere_r));
	memset(sphere_stacks, 0, sizeof(sphere_stacks));
	memset(sphere_slices, 0, sizeof(sphere_slices));
	memset(properties.x, 0, sizeof(properties.x));
	memset(properties.y, 0, sizeof(properties.y));
	memset(properties.z, 0, sizeof(properties.z));
	memset(properties.rotate_x, 0, sizeof(properties.rotate_x));
	memset(properties.rotate_y, 0, sizeof(properties.rotate_y));
	memset(properties.rotate_z, 0, sizeof(properties.rotate_z));
	memset(properties.scale_x, 0, sizeof(properties.scale_x));
	memset(properties.scale_y, 0, sizeof(properties.scale_y));
	memset(properties.scale_z, 0, sizeof(properties.scale_z));
	memset(properties.texcoords_x, 0, sizeof(properties.texcoords_x));
	memset(properties.texcoords_y, 0, sizeof(properties.texcoords_y));
	memset(properties.texture_path, 0, sizeof(properties.texture_path));

	selectedMeshIndex = 0;
	properties.color[0] = 0.5f;
	properties.color[1] = 0.5f;
	properties.color[2] = 0.5f;
	properties.color[3] = 1.f;
	properties.rotate_x[0] = '0';
	properties.rotate_y[0] = '0';
	properties.rotate_z[0] = '0';
	properties.scale_x[0] = '1';
	properties.scale_y[0] = '1';
	properties.scale_z[0] = '1';
	properties.x[0] = '0';
	properties.y[0] = '0';
	properties.z[0] = '0';
	projection = "perspective";
	view = "perspective";
}

ImGuiSettings::~ImGuiSettings() {

}
