#pragma once
using namespace std;
#include "Vector3D.h"
#include "Matrix.h"
#include "Math.h"
#include <gl/freeglut.h>
#include <gl/freeglut.h>
#include <vector>
#include "structs.cpp"

class Cylinder
{
public:
	Cylinder(float base, float top, float height, float slices, float stacks, Vector3D position = Vector3D(0.f, 0.f, 0.f), vector<float> color = { 0.5f, 0.5f, 0.5f });
	~Cylinder();
	float scale;
	float base;
	float top;
	float height;
	float stacks;
	float slices;
	Vector3D position;
	void setPosition(Vector3D vec);
	void setColor(vector<float> col);
	void updateScale(float f);
	void Render();
	vector<Vector3D> verts;
	vector<Vertex> GetVertices();
	vector<unsigned int> GetIndices();

private:
	vector<float> color;
	vector<Vector3D> discPointsBottom;
	vector<Vector3D> discPointsTop;
};

