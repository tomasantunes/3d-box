using namespace std;
#include "Vector3D.h"
#include "Matrix.h"
#include "Math.h"
#include <gl/freeglut.h>
#include <vector>
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include "structs.cpp"

class Box {
	public:
		Box(float m_Length = 1, float m_Width = 1, float m_Height = 1, vector<float> color = { 0.5f, 0.5f, 0.5f }, Vector3D position = Vector3D(0.f, 0.f, 0.f));
		~Box();
		float Volume();
		float SurfaceArea();
		float scale;
		vector<Vector3D> verts;
		vector<Vector3D> Verts();
		Vector3D position;
		Vector3D Center();
		void setPosition(Vector3D vec);
		void setColor(vector<float> col);
		void updateScale(float f);
		void Render();
		bool selected;
		bool collidesWithPoint(Vector3D point);
		vector<Vertex> GetVertices();
		vector<unsigned int> GetIndices();
		vector<unsigned int> GetIndicesQuads();

	private:
		float m_Length;
		float m_Width;
		float m_Height;
		vector<float> color;
		
};

